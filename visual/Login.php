<?php 
session_start();
if(!isset($_SESSION['sessao'])){
	echo "
		<!DOCTYPE html>
		<html lang='pt-br'>
			<head>
			    <meta charset='utf-8' />
                <title>Login-ReUp</title>
                <meta name='viewport' content='width=device-width, initial-scale=1'> 				 				
				<link rel='stylesheet' type='text/css' href='css/log.css'>										
				<link rel='icon' type='imagem/png' href='imagem/Logo.svg.png'>		
			</head>			  				  		
			<body class='fundo'>			
			<div class='float-left'>
				<h2 class='re branco'>Re</h2>
				<h2 class='up branco'>Up</h2>
			</div>
			<div class='container'>						
				<form class='form vertical-align' action='../controle/verificar.php' method='post'>
					<p class='login'>Login</p>
					<div class='form-group'>
				    	<label for='exampleInputEmail1'>Endereço de email</label>
				    	<input type='email' class='form-control' name='email' id='exampleInputEmail1' aria-describedby='emailHelp' placeholder='E-mail' required>				    
				  </div>
				  <div class='form-group'>
				    	<label for='exampleInputPassword1'>Senha</label>
				    	<input type='password' class='form-control' name='senha' id='exampleInputPassword1' placeholder='Senha' required>
				  </div>
				  <div class='form-group'>
				    Não é cadastrado? <a href='Cadastro.php'>Clique para se cadastrar.</a>
				  </div>
				  <button type='submit' class='btn'>Enviar</button>
				</form> 			
			</div>";
			if(isset($_SESSION['erro'])){
				echo"<script src='js/sweet.js'></script>";
				echo"<script>swal('Erro:','Email ou senha incorretos','error');</script>";
				session_destroy();
			}
			echo"
			</body>
		</html>
	";
}else{
	$hash=md5($_SESSION['sessao']);
	header("Location: Principal.php?n0w3={$hash}");
}
 ?>
