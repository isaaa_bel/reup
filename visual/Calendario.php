<?php
require_once("../modelo/CalendarioModelo.php");
require_once("../controle/UsuarioControle.php");
require_once("../controle/CalendarioControle.php");
$controle = new CalendarioControle();
$controleUser=new ControleUsuario();
$item=$controleUser->selecionarTodos();
$calendario = $controle->selecionar();
foreach($item as $atual){
    $mail=md5($atual['email']);
    if($mail==$_GET['n0w3']){
        $mail=$atual['email'];
        break;
    }
}
session_start();
if(!isset($_SESSION['log'])){
    $_SESSION['log']=true;
}
if(isset($_SESSION['sessao'])){
    echo "
  	<!DOCTYPE html>
  	<html>
  	<head>
        <title>Lembretes</title>
  		<meta charset='utf-8'>
  		<meta lang='pt-br'>
  		<meta name='viewport' content='width=device-width, initial-scale=1'>
        <link rel='stylesheet' href='css/bootstrapi.min.css' integrity='sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO' crossorigin='anonymous'> 
        <link rel='stylesheet' href='css/geral.css'>
        <link rel='icon' type='imagem/png' href='imagem/Logo.svg.png'>
  	</head>
  	<body>
  		<header>
            <nav class='navbar navbar-expand-md navbar-light fixed-top' id='a'>
            <a class='navbar-brand' href='../index.php' id='cor'><img src='imagem/Logo.svg.png' width='40'>eUp</a>
                <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarCollapse' aria-controls='navbarCollapse' aria-expanded='false' aria-label='Toggle navigation'>
                    <span class='navbar-toggler-icon'></span>
                </button>
                <div class='collapse navbar-collapse' id='navbarCollapse'>
                    <ul class='navbar-nav mr-auto'>
                        <li class='nav-item active'>
                            <a class='nav-link' id='cor' href='Principal.php?n0w3={$_GET["n0w3"]}'>Projetos</a>
                        </li>
                        <li class='nav-item active'>
                            <a class='nav-link' id='cor' href='Quiz.php?n0w3={$_GET["n0w3"]}'>Quiz</a>
                        </li>
                        <li class='nav-item dropdown'>
            <a id='cor' class='nav-link dropdown-toggle' href='#' id='navbarDropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                Mais
            </a>
            <div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
                <a id='cor' class='dropdown-item' href='Ajuda.php?n0w3={$_GET["n0w3"]}'>Ajuda</a>
                <a id='cor' class='dropdown-item' href='Configurar.php?n0w3={$_GET["n0w3"]}'>Configurações</a>
                <a id='cor' class='dropdown-item' href='../controle/sair.php'>Sair</a>
            </div>
            </li>
                    </ul>
                </div>
            </nav>
        </header>        
        <br><br><br><br>
        <div class='container'>                         
            <center><img src='imagem/lembrete.png' width='220'>
            <br /></center>            
            <br>
        
";

foreach ($calendario as $value){
    $idH=md5($value->getId());
    $email=md5($value->getUsuario());
    if($email==$_GET['n0w3']){
        echo"
        <div class='table-responsive'>
            <table class='table table-bordered'>
                <thead>
                    <tr>
                        <th scope='col'>Data</th>
                        <th scope='col'>Assunto</th>
                        <th scope='col'>Mensagem</th>
                        <th scope='col'>Deletar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope='row'>{$value->getDataa()}</th>
                        <td>{$value->getAssunto()}</td>
                        <td>{$value->getMensagem()}</td>
                        <td><a href='../controle/deletar.php?id={$idH}& n0w3={$_GET['n0w3']}'>Deletar</a></td>
                    </tr>
                </tbody>
            </table>
        ";
        if($value->getConcluir() == "false"){
                echo "      
                    <form action='../controle/concluir.php' method='POST'>
                        <input type='hidden' name='n0w3' value='{$_GET['n0w3']}' />
                        <input type='hidden' name='id' value='{$value->getId()}' />
                        <input type='hidden' name='concluido' value='true' />
                        <button type='submit' class='btn' id='btn' >Concluir!</button>
                    </form>
                </div>
                <br />     
                ";
            }else{
               echo "      
                    <button type='submit' class='btn' style='background-color:#AEC6CF;' id='btn' >Concluido!</button>
                    </div>
                    <br />    
                ";     
            }    
    }
}
echo"
    <br />
  	<div class='container'>
  	<center>           
            <div id='carouselExampleControls' class='carousel slide' data-ride='carousel' data-interval='0' >
            <div class='carousel-inner'>
                <div id='slide1' class='carousel-item active'>
                    <div class='d-flex align-items-center p-3 my-3 text-white-50 rounded shadow-sm' style='background-color: #00ccff;'>
                    <img src='imagem/sino.png' alt='' width='80' id='he'>
                    <div class='lh-100'>
                        <h3 class='mb-0 text-dark lh-100'>Anotar lembrete</h3>          
                    </div>
                    </div>
                    <form method='post' action='../controle/salvar.php' class='form' id='divL'>
                        <div class='my-3 p-3 bg-white rounded shadow-sm'>
                            <h5 class='border-bottom border-gray pb-2 mb-0'>Qual o Assunto e a Mensagem?</h5>
                            <br /><br />
                            <div class='form-row'>
                                <div class='form-group col-md-6'>
                                    <input type='text' name='assunto' class='form-control' id='assunto' placeholder='Título Lembrete' required>
                                </div>
                                <div class='form-group col-md-6'>    
                                    <input type='text' class='form-control' id='mensagem' name='mensagem' placeholder='Mensagem' required>
                                    <br />
                                   <a class='carousel-control-next' href='#carouselExampleControls' data-slide='next' id='tL'>
                                    Continuar            
                                    <span class='sr-only'>Next</span>              
                                    </a>
							     </div>
							</div>
							<br /><br />

                        </div>
                </div>

              
              <div class='carousel-item'>
                    <div class='carousel-item active'>
                    <div class='d-flex align-items-center p-3 my-3 text-white-50 rounded shadow-sm' style='background-color: #00ccff;'>
                    <img src='imagem/sino.png' alt='' width='80' id='he'>
                    <div class='lh-100'>
                        <h3 class='mb-0 text-dark lh-100'>Anotar lembrete</h3>          
                    </div>
                    </div>
                    
                        <div class='my-3 p-3 bg-white rounded shadow-sm'>
                            <h5 class='border-bottom border-gray pb-2 mb-0'>Qual a Data?</h5>
                            <br /><br />
                            <div class='form-row'>
                                <div class='col'>
                                  <input type='date' class='data' name='data' required>
                                  <br /><br />
                                </div>
                                <br /><br />
                            </div>
                        </div>
                        <input type='hidden' name='email' value='{$mail}' />
                        <input type='hidden' name='n0w3' value='{$_GET['n0w3']}' />
                        <input type='hidden' name='concluir' value='false' />
                            <br /><br />
                        </div>
                    <button type='submit' class='btn' id='btn' >Enviar</button>    
                </form>
              </div>
            </div>            
            <br /><br /><br />
          </div>
          </center>
           
    </div>
    </body>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
        <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js' integrity='sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl' crossorigin='anonymous'></script>
        <script type='text/javascript' src='js/jquery.min.js' ></script>
        <script src='js/sweet.js'></script>
        <script src='js/calendario.js'></script>
</html>
";
if(isset($_SESSION['erro'])){
    if($_SESSION['erro']){
        echo"<script>swal('Ops','A execução resultou em erros, tente novamente','error');</script>";
    }
    unset($_SESSION['erro']);
}
}else{
    header("Location: Login.php");
}
 ?>
