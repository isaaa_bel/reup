$("#addLinha").click(function(){
    let usuario=$("#addLinha").val();
    let projeto=$("#addLinha").attr("class");
    $.ajax({url: "../controle/addLinha.php?usuario="+usuario+"&projeto="+projeto, success:function(resultado){
        location.reload();
    }});
});
$("#delP").click(function(){
    let idP=$("#delP").prop("title");
    let n0w3=$("#delP").val();
    swal({
        title:"Atenção",
        icon:"warning",
        text:"Você tem certeza que deseja deletar este projeto?",
        buttons:["Não","Sim"],
        dangerMode:true
    }).then(function(isConfirm){
        if(isConfirm){
            location.href="../controle/deletarProjeto.php?n0w3="+n0w3+"&id="+idP;
        }
    });
});
