let btt=document.getElementById("clickDel");
let email=document.getElementById("emailDel");
btt.onclick=function(){
    swal({
        title: "Atenção",
        icon: "warning",
        text: "Você tem certeza que deseja deletar sua conta?",
        buttons: ["Não","Sim"],
        dangerMode: true
    }).then(function(isConfirm){
        if(isConfirm){
            window.location.href="../controle/deletarConta.php?email="+email.value;
        }
    });
};
