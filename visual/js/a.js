//Editar na tabela
$(function () {
    $("td").dblclick(function () {
        var conteudoOriginal = $(this).text();
         
        $(this).addClass("celulaEmEdicao");
        $(this).html("<input id='teste' type='text' value='" + conteudoOriginal + "' />");        
        document.getElementById('teste').style.borderRadius = '10px';
        $(this).children().first().focus();
 
        $(this).children().first().keypress(function (e) {
            if (e.which == 13) {
                var novoConteudo = $(this).val();
                if(novoConteudo==""){
                    $(this).parent().text("Espaço em branco não pode");
                }else{
                    $(this).parent().text(novoConteudo);
                }
                $(this).parent().removeClass("celulaEmEdicao");
            }
        });
         
    $(this).children().first().blur(function(){
        $(this).parent().text(conteudoOriginal);
        $(this).parent().removeClass("celulaEmEdicao");
    });
    });
