let botao = document.getElementById("cadastrar");
let password = document.getElementById("senha");
   confirm_password = document.getElementById("confSenha");

function validatePassword() {
  if (password.value != confirm_password.value) {
    swal("Atenção:","Sua senha deve ser a mesma","warning");
    confirm_password.setCustomValidity("Sua senha deve ser a mesma");
  } else {
    confirm_password.setCustomValidity('');
  }
}

botao.onmouseover = validatePassword;
botao.onfocus = validatePassword;

//Fazer verificação de email já existente.
let inputEmail = document.getElementById("email");
inputEmail.onblur = function () {
	let ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.response.length<11) {
                swal("Atenção:","Email já cadastrado","warning");
            }
        }
    };
    ajax.open("GET","../controle/testEmail.php?email="+inputEmail.value,true);
    ajax.send();
};

