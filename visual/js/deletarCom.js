let id=document.getElementById("id");
document.getElementById("body").onload=function(){
    swal({
        title:"Atenção",
        text:"Você tem certeza que deseja apagar esse comentário?",
        icon:"warning",
        buttons:['Não','Sim'],
        dangerMode:true
    }).then(function(isConfirm){
        if(isConfirm){
            window.location.href="../controle/deletarCom.php?id="+id.value;
        }else{
            window.location.href="adm.php";
        }
    });
};
