jQuery(function ($) {
    $(".sidebar-dropdown > a").click(function() {
  $(".sidebar-submenu").slideUp(200);
  if (
    $(this)
      .parent()
      .hasClass("active")
  ) {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .parent()
      .removeClass("active");
  } else {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .next(".sidebar-submenu")
      .slideDown(200);
    $(this)
      .parent()
      .addClass("active");
  }
});

$("#close-sidebar").click(function() {
  $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
  $(".page-wrapper").addClass("toggled");
});  
});

//Editar na tabela
$(function () {
    $("td").dblclick(function () {
        let conteudoOriginal = $(this).text();
        let id=$(this).prop("title");
        let field=$(this).attr("class");
        //$(this).addClass("celulaEmEdicao");
        $(this).html("<input id='teste' type='text' value='" + conteudoOriginal + "' />");        
        a = document.getElementById('teste').style.borderRadius = '10px';        
        $(this).children().first().focus();
        $(this).children().first().keypress(function (e) {
            if (e.which == 13) {
                let novoConteudo = $(this).val();
                $(this).parent().text(novoConteudo);
                //$(this).parent().removeClass("celulaEmEdicao");
                $.ajax({url: "../controle/atualizarTabela.php?id="+id+"&campo="+field+"&valor="+novoConteudo, success: function(resultado){
                    location.reload();
                }});
            }
        });
         
    $(this).children().first().blur(function(){
        $(this).parent().text(conteudoOriginal);
        //$(this).parent().removeClass("celulaEmEdicao");
    });
    });
});

let a = document.getElementById("a");
let n0w3 = document.getElementById("n0w3");
  a.onclick = function(){
    swal({
      title:"Atenção",
      text:"Você tem certeza que deseja apagar seu projeto?",
      icon:"warning",
      buttons:['Não','Sim'],
      dangerMode:true
  }).then(function(isConfirm){
      if(isConfirm){
          window.location.href="../controle/deletarCom.php?id="+id.value;
      }else{
          window.location.href="Projetos.php?n0w3="+n0w3;
      }
  });
  }
