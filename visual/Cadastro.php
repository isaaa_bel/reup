<?php
session_start();
echo "
    <!DOCTYPE html>
    <html lang='pt-br'>    
    <head>
        <title>Cadastre-se</title>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <link rel='stylesheet' type='text/css' href='css/log.css'>
        <link rel='icon' type='imagem/png' href='imagem/Logo.svg.png'>
    </head>
    <body class='fundo'>    
            <div class='float-left'>
                <h3 class='reC branco'>Re</h3>
                <h2 class='upC branco'>Up</h2>
            </div>
            <div class='container'>                        
                <form class='form vertical-alignC' action='../controle/cadastro.php'  method='post'>                
                    <p class='cad'>Cadastre-se</p>
                    <div class='form-group'>                        
                        <label for='nome'>Nome ou Apelido</label>
                        <input type='text' id='nome' class='form-control' name='nome' placeholder='Como prefere ser chamado' required>
                        <br>
                        <div>
                        <label for='exampleInputEmail1'>E-mail</label>
                        <input type='email' name='email' class='form-control' id='email' aria-describedby='emailHelp' placeholder='Ex: cocozinho@reup.com' required><div>
                        <br>
                        <div>
                        <label for='s'>Senha</label>
                        <input type='password' class='form-control' name='senha' id='senha' placeholder='Senha' required>
                        <br>
                        <label for='sc'>Confirmar Senha</label>
                        <input type='password' class='form-control' name='confSenha' id='confSenha' placeholder='Confirmar Senha' required>
                        <br/>   
                        <div class='form-group'>
                            Já está cadastrado? <a href='Login.php'>Clique para logar-se.</a>
                        </div>
                    <button type='submit' id='cadastrar' class='btn'>Cadastrar</button>                    
                </form>        
        </div>        
    </body>
    <script src='js/sweet.js'></script>
    <script type='text/javascript' src='js/teste.js' ></script>
    <script type='text/javascript' src='js/jquery.min.js' ></script>
    </html>
";
if(isset($_SESSION['erroC'])){
    if($_SESSION['erroC']){
        echo"<script>swal('Ops','Este email já está cadastrado','error');</script>";
    }else{
        echo"<script>swal('Ops','As senhas devem ser as mesmas','error');</script>";
    }
    unset($_SESSION['erroC']);
}
if(isset($_SESSION['erroB'])){
    echo"<script>swal('Desculpe','Estamos com problemas no servidor','info');</script>";
    unset($_SESSION['erroB']);
}
?>
