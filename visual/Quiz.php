<?php
session_start();
if(!isset($_SESSION['log'])){
    $_SESSION['log']=true;
}
if(isset($_SESSION['sessao'])){
    echo "
<!DOCTYPE html>
<html lang='pt-br'>
<head>
    <title>Quiz</title>
    <meta charset='utf-8'>
    <link rel='stylesheet' href='menu.css' />
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' href='css/bootstrap.css' crossorigin='anonymous'>
    <link rel='stylesheet' href='css/geral.css'>
    <link rel='icon' type='imagem/png' href='imagem/Logo.svg.png'>
</head>
<body id='teste'>
    <!--Carregamento-->
    <div id='loading' class='vertical-align fadeIn'>
        <center>
            <img id='quiz' src='imagem/quiz.gif' style='width:40%;height:auto;' />
        </center>
    </div>
    <!--Conteudo-->
    <div id='conteudo' style='display: none'>
        <nav class='navbar navbar-expand-lg navbar-light' id='a'>
            <a class='navbar-brand' href='../index.php'><img src='imagem/Logo.svg.png' width='40' />eUp</a>
            <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarNavDropdown' aria-controls='navbarNavDropdown' aria-expanded='false' aria-label='Toggle navigation'>
                <span class='navbar-toggler-icon'></span>
            </button>
            <div class='collapse navbar-collapse' id='navbarNavDropdown'>
                <ul class='navbar-nav'>
                    <li class='nav-item'>
                        <a class='nav-link' id='cor' href='Principal.php?n0w3={$_GET["n0w3"]}'>Projetos</a>
                    </li>
                    <li class='nav-item dropdown'>
                        <a id='cor' class='nav-link dropdown-toggle' href='#' id='navbarDropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                            Mais
                        </a>
                        <div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
                            <a id='cor' class='dropdown-item' href='Ajuda.php?n0w3={$_GET["n0w3"]}'>Ajuda</a>
                            <a id='cor' class='dropdown-item' href='Configurar.php?n0w3={$_GET["n0w3"]}'>Configurações</a>
                            <a id='cor' class='dropdown-item' href='../controle/sair.php'>Sair</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>";
if(!isset($_SESSION['respostas'])){
echo"
        <div class='container'>
            <div class='d-flex align-items-center p-3 my-3 text-white-50 rounded shadow-sm' id='aa'>
                <img src='imagem/quiz.gif' alt='' width='80' id='he' />
                <div class='lh-100'>
                    <h3 class='mb-0 text-dark lh-100'>Hora do Quiz</h3>          
                </div>
            </div>
            <!--Primeiro Form-->
            <form method='post' action='../controle/quiz.php' class='form'>
                <!--Primeira pergunta-->
                <div class='my-3 p-3 bg-white rounded shadow-sm'>
                    <h5 class='border-bottom border-gray pb-2 mb-0'>Você estuda em que período?</h5>
                    <div class='media pt-3'>
                        <div class='form-check'>
                            <input class='form-check-input' required type='radio' name='r' id='r' value='manha'>
                            <label class='form-check-label' for='r'>Manhã</label>                                                    
                            <br>
                            <input class='form-check-input' type='radio' name='r' id='r1' value='tarde'>
                            <label class='form-check-label' for='r1'>Tarde</label>                                    
                            <br>                            
                            <input class='form-check-input' type='radio' name='r' id='r3' value='noite'>
                            <label class='form-check-label' for='r3'>Noite</label>                                    
                            <br>
                            <input class='form-check-input' type='radio' name='r' id='r4' value='integral'>
                            <label class='form-check-label' for='r4'>Integral</label>                                    
                            <br>
                            <input class='form-check-input' type='radio' name='r' id='r5' value='trabalha'>
                            <label class='form-check-label' for='r5'>Somente Trabalha</label>                                    
                        </div>              
                    </div>          
                </div>
                <!--Segunda pergunta-->
                <div class='my-3 p-3 bg-white rounded shadow-sm'>
                    <h5 class='border-bottom border-gray pb-2 mb-0'>Para que você quer organizar seu tempo?</h5>
                    <div class='media pt-3'>
                        <div class='form-check'>
                            <input class='form-check-input' required type='radio' name='r2' id='r21' value='vestibular'>
                            <label class='form-check-label' for='r21'>Focar para passar em algum vestibular</label>                                                    
                            <br>
                            <input class='form-check-input' type='radio' name='r2' id='r22' value='aproveitar'>
                            <label class='form-check-label' for='r22'>Mais tempo para aproveitar com pessoas que gosto</label>                                    
                            <br>                            
                            <input class='form-check-input' type='radio' name='r2' id='r23' value='lazer'>
                            <label class='form-check-label' for='r23'>Tempo para lazer, esporte ou parecido</label>                                    
                            <br>                        
                            <input class='form-check-input' type='radio' name='r2' id='r24' value='esbalhar'>
                            <label class='form-check-label' for='r24'>Para estudar e Trabalhar</label>                                    
                        </div>              
                    </div>          
                </div>
                <input type='hidden' value='{$_GET["n0w3"]}' name='n0w3' />
                <input type='submit' value='Continuar' id='continuar' />
            </form>
        </div>
";
}else{
    if(!isset($_SESSION['final'])){
        if($_SESSION['respostas'][0]=='manha'){
            echo"
                <div class='container'>
                    <div class='d-flex align-items-center p-3 my-3 text-white-50 rounded shadow-sm' id='aa'>
                        <img src='imagem/quiz.gif' alt='' width='80' id='he' />
                        <div class='lh-100'>
                            <h3 class='mb-0 text-dark lh-100'>Hora do Quiz</h3>          
                        </div>
                    </div>
                    <!-- Segundo Form -->
                    <form method='post' action='../controle/quiz.php' class='form'>
                        <!-- Terceira pergunta -->
                        <div class='my-3 p-3 bg-white rounded shadow-sm'>
                            <h5 class='border-bottom border-gray pb-2 mb-0'>Escolha um desses horários para começar:</h5>
                            <div class='media pt-3'>
                                <div class='form-check'>                              
                                    <input required class='form-check-input' type='radio' name='r4' value='13' id='r'>
                                    <label class='form-check-label' for='r'>13:00</label>                                    
                                    <br>
                                    <input class='form-check-input' type='radio' name='r4' value='14' id='r2'>
                                    <label class='form-check-label' for='r2'>14:00</label>                                    
                                    <br>
                                    <input class='form-check-input' type='radio' name='r4' value='18' id='r3'>
                                    <label class='form-check-label' for='r3'>18:00</label>                                    
                                    <br>
                                    <input class='form-check-input' type='radio' name='r4' value='19' id='r4'>
                                    <label class='form-check-label' for='r4'>19:00</label>                                    
                                </div>              
                            </div>          
                        </div>
                            <div class='my-3 p-3 bg-white rounded shadow-sm'>
                                <h5 class='border-bottom border-gray pb-2 mb-0'>Quanto tempo você pretende dedicar a cada prioridade:</h5>
                                <div class='media pt-3'>
                                    <div class='form-check'>
                                        <input required class='form-check-input' type='radio' name='r5' value='3' id='r21'>
                                        <label class='form-check-label' for='r21'>3 horas</label>                                                    
                                        <br>
                                        <input class='form-check-input' type='radio' name='r5' value='4' id='r22'>
                                        <label class='form-check-label' for='r22'>4 horas</label>                                    
                                        <br>                            
                                        <input class='form-check-input' type='radio' name='r5' value='5' id='r23'>
                                        <label class='form-check-label' for='r23'>5 horas</label>                                    
                                        <br>                                  
                                    </div>              
                                </div>          
                            </div>
                        
                            <input type='hidden' value='{$_GET["n0w3"]}' name='n0w3' />
                            <input type='submit' value='Continuar' id='continuar' />
                    </form>
                </div>
            ";
        }else if($_SESSION['respostas'][0]=='tarde'){
            echo"
                <div class='container'>
                    <div class='d-flex align-items-center p-3 my-3 text-white-50 rounded shadow-sm' id='aa'>
                        <img src='imagem/quiz.gif' alt='' width='80' id='he' />
                        <div class='lh-100'>
                            <h3 class='mb-0 text-dark lh-100'>Hora do Quiz</h3>          
                        </div>
                    </div>
                    <!-- Segundo Form -->
                    <form method='post' action='../controle/quiz.php' class='form'>
                        <!-- Terceira pergunta -->
                        <div class='my-3 p-3 bg-white rounded shadow-sm'>
                            <h5 class='border-bottom border-gray pb-2 mb-0'>Escolha um desses horários para começar:</h5>
                            <div class='media pt-3'>
                                <div class='form-check'>                              
                                    <input required class='form-check-input' type='radio' name='r4' value='6' id='r'>
                                    <label class='form-check-label' for='r'>6:00</label>                                                    
                                    <br>
                                    <input class='form-check-input' type='radio' name='r4' value='7' id='rt2'>
                                    <label class='form-check-label' for='rt2'>7:00</label>                                    
                                    <br>                            
                                    <input class='form-check-input' type='radio' name='r4' value='18' id='rt3'>
                                    <label class='form-check-label' for='rt3'>18:00</label>                                    
                                    <br>
                                    <input class='form-check-input' type='radio' name='r4' value='19' id='rt4'>
                                    <label class='form-check-label' for='rt4'>19:00</label>                                    
                                </div>              
                            </div>    
                            </div>      
                            <div class='my-3 p-3 bg-white rounded shadow-sm'>
                                <h5 class='border-bottom border-gray pb-2 mb-0'>Quanto tempo você pretende dedicar a cada prioridade:</h5>
                                <div class='media pt-3'>
                                    <div class='form-check'>
                                        <input required class='form-check-input' type='radio' name='r5' value='3' id='rt'>
                                        <label class='form-check-label' for='rt'>3 horas</label>                                                    
                                        <br>
                                        <input class='form-check-input' type='radio' name='r5' value='4' id='rt1'>
                                        <label class='form-check-label' for='rt1'>4 horas</label>                                    
                                        <br>                            
                                        <input class='form-check-input' type='radio' name='r5' value='5' id='rt23'>
                                        <label class='form-check-label' for='rt23'>5 horas</label>                                    
                                        <br>                                  
                                    </div>              
                                </div>          
                            </div>                        
                            <input type='hidden' value='{$_GET["n0w3"]}' name='n0w3' />
                            <input type='submit' value='Continuar' id='continuar' />
                    </form>
                </div>
            ";
        }else if($_SESSION['respostas'][0]=='noite'){
            echo"
                <div class='container'>
                    <div class='d-flex align-items-center p-3 my-3 text-white-50 rounded shadow-sm' id='aa'>
                        <img src='imagem/quiz.gif' alt='' width='80' id='he' />
                        <div class='lh-100'>
                            <h3 class='mb-0 text-dark lh-100'>Hora do Quiz</h3>          
                        </div>
                    </div>
                    <!-- Segundo Form -->
                    <form method='post' action='../controle/quiz.php' class='form'>
                        <!-- Terceira pergunta -->
                        <div class='my-3 p-3 bg-white rounded shadow-sm'>
                            <h5 class='border-bottom border-gray pb-2 mb-0'>Escolha um desses horários para começar:</h5>
                            <div class='media pt-3'>
                                <div class='form-check'>                              
                                    <input required class='form-check-input' type='radio' name='r4' value='6' id='r'>
                                    <label class='form-check-label' for='r'>6:00</label>                                                    
                                    <br>
                                    <input class='form-check-input' type='radio' name='r4' value='7' id='rn'>
                                    <label class='form-check-label' for='rn'>7:00</label>                                    
                                    <br>                            
                                    <input class='form-check-input' type='radio' name='r4' value='13' id='rn2'>
                                    <label class='form-check-label' for='rn2'>13:00</label>                                    
                                    <br>
                                    <input class='form-check-input' type='radio' name='r4' value='14' id='rn3'>
                                    <label class='form-check-label' for='rn3'>14:00</label>                                    
                                </div>              
                            </div>  
                            </div>        
                            <div class='my-3 p-3 bg-white rounded shadow-sm'>
                                <h5 class='border-bottom border-gray pb-2 mb-0'>Quanto tempo você pretende dedicar a cada prioridade:</h5>
                                <div class='media pt-3'>
                                    <div class='form-check'>
                                        <input required class='form-check-input' type='radio' name='r5' value='3' id='rn1'>
                                        <label class='form-check-label' for='rn1'>3 horas</label>                                                    
                                        <br>
                                        <input class='form-check-input' type='radio' name='r5' value='4' id='rn4'>
                                        <label class='form-check-label' for='rn4'>4 horas</label>                                    
                                        <br>                            
                                        <input class='form-check-input' type='radio' name='r5' value='5' id='rn5'>
                                        <label class='form-check-label' for='rn5'>5 horas</label>                                    
                                        <br>                                  
                                    </div>              
                                </div>          
                            </div>                        
                            <input type='hidden' value='{$_GET["n0w3"]}' name='n0w3' />
                            <input type='submit' value='Continuar' id='continuar' />
                    </form>
                </div>
            ";
        }else if($_SESSION['respostas'][0]=='integral'){
            echo"
                <div class='container'>
                    <div class='d-flex align-items-center p-3 my-3 text-white-50 rounded shadow-sm' id='aa'>
                        <img src='imagem/quiz.gif' alt='' width='80' id='he' />
                        <div class='lh-100'>
                            <h3 class='mb-0 text-dark lh-100'>Hora do Quiz</h3>          
                        </div>
                    </div>
                    <!-- Segundo Form -->
                    <form method='post' action='../controle/quiz.php' class='form'>
                        <!-- Terceira pergunta -->
                        <div class='my-3 p-3 bg-white rounded shadow-sm'>
                            <h5 class='border-bottom border-gray pb-2 mb-0'>Escolha um desses horários para começar:</h5>
                            <div class='media pt-3'>
                                <div class='form-check'>                              
                                    <input required class='form-check-input' type='radio' name='r4' value='18' id='r'>
                                    <label class='form-check-label' for='r'>18:00</label>                                    
                                    <br>
                                    <input class='form-check-input' type='radio' name='r4' value='19' id='r2'>
                                    <label class='form-check-label' for='r2'>19:00</label>                                    
                                </div>              
                            </div>   
                            </div>       
                            <div class='my-3 p-3 bg-white rounded shadow-sm'>
                                <h5 class='border-bottom border-gray pb-2 mb-0'>Quanto tempo você pretende dedicar a cada prioridade:</h5>
                                <div class='media pt-3'>
                                    <div class='form-check'>
                                        <input required class='form-check-input' type='radio' name='r5' value='3' id='ri'>
                                        <label class='form-check-label' for='ri'>3 horas</label>                                                    
                                        <br>
                                        <input class='form-check-input' type='radio' name='r5' value='4' id='ri1'>
                                        <label class='form-check-label' for='ri1'>4 horas</label>                                    
                                        <br>                            
                                        <input class='form-check-input' type='radio' name='r5' value='5' id='ri2'>
                                        <label class='form-check-label' for='ri2'>5 horas</label>                                    
                                        <br>                                  
                                    </div>              
                                </div>          
                            </div>                        
                            <input type='hidden' value='{$_GET["n0w3"]}' name='n0w3' />
                            <input type='submit' value='Continuar' id='continuar' />
                    </form>
                </div>
            ";
        }else{
            if($_SESSION['respostas'][0]=='trabalha'){
                echo"
            <div class='container'>
                <div class='d-flex align-items-center p-3 my-3 text-white-50 rounded shadow-sm' id='aa'>
                    <img src='imagem/quiz.gif' alt='' width='80' id='he' />
                    <div class='lh-100'>
                        <h3 class='mb-0 text-dark lh-100'>Hora do Quiz</h3>          
                    </div>
                </div>
                <!-- Segundo Form -->
                <form method='post' action='../controle/quiz.php' class='form'>
                    <!-- Terceira pergunta -->
                    <div class='my-3 p-3 bg-white rounded shadow-sm'>
                        <h5 class='border-bottom border-gray pb-2 mb-0'>Escolha um desses horários para começar:</h5>
                        <div class='media pt-3'>
                            <div class='form-check'>                              
                                <input required class='form-check-input' type='radio' name='r4' value='6' id='r'>
                                <label class='form-check-label' for='r'>6:00</label>                                                    
                                <br>
                                <input class='form-check-input' type='radio' name='r4' value='7' id='r1'>
                                <label class='form-check-label' for='r1'>7:00</label>                                    
                                <br>                            
                                <input class='form-check-input' type='radio' name='r4' value='13' id='r2'>
                                <label class='form-check-label' for='r2'>13:00</label>                                    
                                <br>
                                <input class='form-check-input' type='radio' name='r4' value='14' id='r3'>
                                <label class='form-check-label' for='r3'>14:00</label>                                    
                                <br>
                                <input class='form-check-input' type='radio' name='r4' value='18' id='r4'>
                                <label class='form-check-label' for='r4'>18:00</label>                                    
                                <br>
                                <input class='form-check-input' type='radio' name='r4' value='19' id='r5'>
                                <label class='form-check-label' for='r5'>19:00</label>                                    
                            </div>              
                        </div> 
                        </div>         
                        <div class='my-3 p-3 bg-white rounded shadow-sm'>
                            <h5 class='border-bottom border-gray pb-2 mb-0'>Quanto tempo você pretende dedicar a cada prioridade:</h5>
                            <div class='media pt-3'>
                                <div class='form-check'>
                                    <input required class='form-check-input' type='radio' name='r5' value='3' id='th1'>
                                    <label class='form-check-label' for='th1'>3 horas</label>                                                    
                                    <br>
                                    <input class='form-check-input' type='radio' name='r5' value='4' id='th2'>
                                    <label class='form-check-label' for='th2'>4 horas</label>                                    
                                    <br>                            
                                    <input class='form-check-input' type='radio' name='r5' value='5' id='th3'>
                                    <label class='form-check-label' for='th3'>5 horas</label>                                    
                                    <br>                                  
                                </div>              
                            </div>          
                        </div>                    
                        <input type='hidden' value='{$_GET["n0w3"]}' name='n0w3' />
                        <input type='submit' value='Continuar' id='continuar' />
                </form>
            </div>
        ";
            }
        }
    }else{
        // RESULTADO DAS PERGUNTAS
        echo "<div class='container'>
                <div class='my-3 p-3 bg-white rounded shadow-sm'>
                <h2 class='border-bottom border-gray pb-2 mb-0'>Resultado</h2>
                <br>
        ";

            if($_SESSION['respostas'][0]=='manha'){
                if($_SESSION['respostas'][1]=='vestibular'){
                    // Manhã e vestibular
                    if($_SESSION['respostas'][2]=='13'){
                        // Manhã, vestibular e começar 13h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, vestibular, começar 13h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é buscar um vestibular, quer começar às 13h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, vestibular, começar 13h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é buscar um vestibular, quer começar às 13h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                            ";
                        }else{
                            // Manhã, vestibular, começar 13h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é buscar um vestibular, quer começar às 13h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='14'){
                        // Manhã, vestibular e começar 14h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, vestibular, começar 14h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é buscar um vestibular, quer começar às 14h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, vestibular, começar 14h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é buscar um vestibular, quer começar às 14h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                            ";
                        }else{
                            // Manhã, vestibular, começar 14h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é buscar um vestibular, quer começar às 14h e passar 5h estudando.";
                             echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='18'){
                        // Manhã, vestibular e começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, vestibular, começar 18h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é buscar um vestibular, quer começar às 18h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, vestibular, começar 18h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é buscar um vestibular, quer começar às 18h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                            ";
                        }else{
                            // Manhã, vestibular, começar 18h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é buscar um vestibular, quer começar às 18h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }else{
                        // Manhã, vestibular e começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, vestibular, começar 19h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é buscar um vestibular, quer começar às 19h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, vestibular, começar 19h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é buscar um vestibular, quer começar às 19h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                            ";
                        }else{
                            // Manhã, vestibular, começar 19h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é buscar um vestibular, quer começar às 19h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }
                }else if($_SESSION['respostas'][1]=='aproveitar'){
                    // Manhã e aproveitar
                    if($_SESSION['respostas'][2]=='13'){
                        // Manhã, aproveitar, começar 13h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, aproveitar, começar 13h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é aproveitar, quer começar às 13h e passar 3h nas atividades.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, aproveitar, começar 13h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é aproveitar, quer começar às 13h e passar 4h nas atividades.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Manhã, aproveitar, começar 13h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é aproveitar, quer começar às 13h e passar 5h nas atividades.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='14'){
                        // Manhã, aproveitar, começar 14h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, aproveitar, começar 14h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é aproveitar, quer começar às 14h e passar 3h nas atividades.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, aproveitar, começar 14h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é aproveitar, quer começar às 14h e passar 4h nas atividades.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Manhã, aproveitar, começar 14h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é aproveitar, quer começar às 14h e passar 5h nas atividades.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='18'){
                        // Manhã, aproveitar, começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, aproveitar, começar 18h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é aproveitar, quer começar às 18h e passar 3h nas atividades.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, aproveitar, começar 18h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é aproveitar, quer começar às 18h e passar 4h nas atividades.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Manhã, aproveitar, começar 18h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é aproveitar, quer começar às 18h e passar 5h nas atividades.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }else{
                        // Manhã, aproveitar, começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, aproveitar, começar 19h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é aproveitar, quer começar às 19h e passar 3h nas atividades.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, aproveitar, começar 19h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é aproveitar, quer começar às 19h e passar 4h nas atividades.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Manhã, aproveitar, começar 19h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é aproveitar, quer começar às 19h e passar 5h nas atividades.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }
                }else if($_SESSION['respostas'][1]=='lazer'){
                    // Manhã e lazer
                    if($_SESSION['respostas'][2]=='13'){
                        // Manhã, lazer e começar 13h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, lazer, começar 13h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é lazer, quer começar às 13h e passar 3h em lazer.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, lazer, começar 13h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é lazer, quer começar às 13h e passar 4h em lazer.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Manhã, lazer, começar 13h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é lazer, quer começar às 13h e passar 5h em lazer.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='14'){
                        // Manhã, lazer e começar 14h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, lazer, começar 14h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é lazer, quer começar às 14h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, lazer, começar 14h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é lazer, quer começar às 14h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Manhã, lazer, começar 14h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é lazer, quer começar às 14h e passar 5h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='18'){
                        // Manhã, lazer e começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, lazer, começar 18h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é lazer, quer começar às 18h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, lazer, começar 18h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é lazer, quer começar às 18h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Manhã, lazer, começar 18h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é lazer, quer começar às 18h e passar 5h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }else{
                        // Manhã, lazer e começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, lazer, começar 19h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é lazer, quer começar às 19h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, lazer, começar 19h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é lazer, quer começar às 19h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Manhã, lazer, começar 19h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é lazer, quer começar às 19h e passar 5h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }
                }else{
                    // Manhã e esbalhar
                    if($_SESSION['respostas'][2]=='13'){
                        // Manhã, esbalhar e começar 13h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, esbalhar, começar 13h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é estudar e trabalhar, quer começar às 13h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, esbalhar, começar 13h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é estudar e trabalhar, quer começar às 13h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Manhã, esbalhar, começar 13h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é estudar e trabalhar, quer começar às 13h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='14'){
                        // Manhã, esbalhar e começar 14h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, esbalhar, começar 14h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é estudar e trabalhar, quer começar às 14h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, esbalhar, começar 14h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é estudar e trabalhar, quer começar às 14h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Manhã, esbalhar, começar 14h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é estudar e trabalhar, quer começar às 14h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='18'){
                        // Manhã, esbalhar e começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, esbalhar, começar 18h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é estudar e trabalhar, quer começar às 18h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, esbalhar, começar 18h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é estudar e trabalhar, quer começar às 18h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Manhã, esbalhar, começar 18h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é estudar e trabalhar, quer começar às 18h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }else{
                        // Manhã, esbalhar e começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Manhã, esbalhar, começar 19h e 3h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é estudar e trabalhar, quer começar às 19h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Manhã, esbalhar, começar 19h e 4h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é estudar e trabalhar, quer começar às 19h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Manhã, esbalhar, começar 19h e 5h pra prioridade
                            echo"Você estuda de manhã, sua prioridade é estudar e trabalhar, quer começar às 19h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }
                }
            }else if($_SESSION['respostas'][0]=='tarde'){
                if($_SESSION['respostas'][1]=='vestibular'){
                    // Tarde e vestibular
                    if($_SESSION['respostas'][2]=='6'){
                        // Tarde, vestibular e começar 6h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, vestibular, começar 6h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é passar no vestibular, quer começar às 6h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, vestibular, começar 6h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é passar no vestibular, quer começar às 6h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                            ";
                        }else{
                            // Tarde, vestibular, começar 6h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é passar no vestibular, quer começar às 6h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='7'){
                        // Tarde, vestibular e começar 7h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, vestibular, começar 7h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é passar no vestibular, quer começar às 7h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, vestibular, começar 7h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é passar no vestibular, quer começar às 7h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                            ";
                        }else{
                            // Tarde, vestibular, começar 7h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é passar no vestibular, quer começar às 7h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='18'){
                        // Tarde, vestibular e começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, vestibular, começar 18h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é passar no vestibular, quer começar às 18h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, vestibular, começar 18h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é passar no vestibular, quer começar às 18h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                            ";
                        }else{
                            // Tarde, vestibular, começar 18h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é passar no vestibular, quer começar às 18h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }else{
                        // Tarde, vestibular e começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, vestibular, começar 19h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é passar no vestibular, quer começar às 19h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, vestibular, começar 19h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é passar no vestibular, quer começar às 19h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                            ";
                        }else{
                            // Tarde, vestibular, começar 19h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é passar no vestibular, quer começar às 19h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }
                }else if($_SESSION['respostas'][1]=='aproveitar'){
                    // Tarde e aproveitar
                    if($_SESSION['respostas'][2]=='6'){
                        // Tarde, aproveitar e começar 6h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, aproveitar, começar 6h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é aproveitar, quer começar às 6h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, aproveitar, começar 6h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é aproveitar, quer começar às 6h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Tarde, aproveitar, começar 6h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é aproveitar, quer começar às 6h e passar 5h aproveitando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='7'){
                        // Tarde, aproveitar e começar 7h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, aproveitar, começar 7h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é aproveitar, quer começar às 7h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, aproveitar, começar 7h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é aproveitar, quer começar às 7h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Tarde, aproveitar, começar 7h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é aproveitar, quer começar às 7h e passar 5h aproveitando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='18'){
                        // Tarde, aproveitar e começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, aproveitar, começar 18h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é aproveitar, quer começar às 18h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, aproveitar, começar 18h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é aproveitar, quer começar às 18h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Tarde, aproveitar, começar 18h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é aproveitar, quer começar às 18h e passar 5h aproveitando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }else{
                        // Tarde, aproveitar e começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, aproveitar, começar 19h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é aproveitar, quer começar às 19h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, aproveitar, começar 19h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é aproveitar, quer começar às 19h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Tarde, aproveitar, começar 19h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é aproveitar, quer começar às 19h e passar 5h aproveitando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }
                }else if($_SESSION['respostas'][1]=='lazer'){
                    // Tarde e lazer
                    if($_SESSION['respostas'][2]=='6'){
                        // Tarde, lazer e começar 6h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, lazer, começar 6h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é lazer, quer começar às 6h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, lazer, começar 6h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é lazer, quer começar às 6h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Tarde, lazer, começar 6h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é lazer, quer começar às 6h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='7'){
                        // Tarde, lazer e começar 7h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, lazer, começar 7h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é lazer, quer começar às 7h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, lazer, começar 7h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é lazer, quer começar às 7h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Tarde, lazer, começar 7h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é lazer, quer começar às 7h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='18'){
                        // Tarde, lazer e começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, lazer, começar 18h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é lazer, quer começar às 18h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, lazer, começar 18h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é lazer, quer começar às 18h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Tarde, lazer, começar 18h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é lazer, quer começar às 18h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }else{
                        // Tarde, lazer e começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, lazer, começar 19h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é lazer, quer começar às 19h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, lazer, começar 19h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é lazer, quer começar às 19h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Tarde, lazer, começar 19h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é lazer, quer começar às 19h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }
                }else{
                    // Tarde e esbalhar
                    if($_SESSION['respostas'][2]=='6'){
                        // Tarde, esbalhar e começar 6h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, esbalhar, começar 6h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é estudar e trabalhar, quer começar às 6h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, esbalhar, começar 6h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é estudar e trabalhar, quer começar às 6h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Tarde, esbalhar, começar 6h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é estudar e trabalhar, quer começar às 6h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='7'){
                        // Tarde, esbalhar e começar 7h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, esbalhar, começar 7h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é estudar e trabalhar, quer começar às 7h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, esbalhar, começar 7h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é estudar e trabalhar, quer começar às 7h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Tarde, esbalhar, começar 7h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é estudar e trabalhar, quer começar às 7h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='18'){
                        // Tarde, esbalhar e começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, esbalhar, começar 18h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é estudar e trabalhar, quer começar às 18h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, esbalhar, começar 18h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é estudar e trabalhar, quer começar às 18h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Tarde, esbalhar, começar 18h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é estudar e trabalhar, quer começar às 18h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }else{
                        // Tarde, esbalhar e começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Tarde, esbalhar, começar 19h e 3h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é estudar e trabalhar, quer começar às 19h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Tarde, esbalhar, começar 19h e 4h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é estudar e trabalhar, quer começar às 19h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Tarde, esbalhar, começar 19h e 5h pra prioridade
                            echo"Você estuda à tarde, sua prioridade é estudar e trabalhar, quer começar às 19h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }
                }
            }else if($_SESSION['respostas'][0]=='noite'){
                if($_SESSION['respostas'][1]=='vestibular'){
                    // Noite e vestibular
                    if($_SESSION['respostas'][2]=='6'){
                        // Noite, vestibular e começar 6h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, vestibular, começar 6h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é passar no vestibular, quer começar às 6h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, vestibular, começar 6h e 4h pra prioridade
                            echo"Você estuda à noite, sua prioridade é passar no vestibular, quer começar às 6h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                            ";
                        }else{
                            // Noite, vestibular, começar 6h e 5h pra prioridade
                            echo"Você estuda à noite, sua prioridade é passar no vestibular, quer começar às 6h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='7'){
                        // Noite, vestibular e começar 7h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, vestibular, começar 7h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é passar no vestibular, quer começar às 7h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, vestibular, começar 7h e 4h pra prioridade
                            echo"Você estuda à noite, sua prioridade é passar no vestibular, quer começar às 7h e passar 4h estudando.";
                             echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                            ";
                        }else{
                            // Noite, vestibular, começar 7h e 5h pra prioridade
                            echo"Você estuda à noite, sua prioridade é passar no vestibular, quer começar às 7h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='13'){
                        // Noite, vestibular e começar 13h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, vestibular, começar 13h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é passar no vestibular, quer começar às 13h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, vestibular, começar 13h e 4h pra prioridade
                            echo"Você estuda à noite, sua prioridade é passar no vestibular, quer começar às 13h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                            ";
                        }else{
                            // Noite, vestibular, começar 13h e 5h pra prioridade
                            echo"Você estuda à noite, sua prioridade é passar no vestibular, quer começar às 13h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }else{
                        // Noite, vestibular e começar 14h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, vestibular, começar 14h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é passar no vestibular, quer começar às 14h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";

                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, vestibular, começar 14h e 4h pra prioridade
                            echo"Você estuda à noite, sua prioridade é passar no vestibular, quer começar às 14h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                            ";
                        }else{
                            // Noite, vestibular, começar 14h e 5h pra prioridade
                            echo"Você estuda à noite, sua prioridade é passar no vestibular, quer começar às 14h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }
                }else if($_SESSION['respostas'][1]=='aproveitar'){
                    // Noite e aproveitar
                    if($_SESSION['respostas'][2]=='6'){
                        // Noite, aproveitar e começar 6h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, aproveitar, começar 6h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é aproveitar, quer começar às 6h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, aproveitar, começar 6h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é aproveitar, quer começar às 6h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Noite, aproveitar, começar 6h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é aproveitar, quer começar às 6h e passar 5h aproveitando.";
                             echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='7'){
                        // Noite, aproveitar e começar 7h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, aproveitar, começar 7h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é aproveitar, quer começar às 7h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, aproveitar, começar 7h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é aproveitar, quer começar às 7h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Noite, aproveitar, começar 7h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é aproveitar, quer começar às 7h e passar 5h aproveitando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='13'){
                        // Noite, aproveitar e começar 13h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, aproveitar, começar 13h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é aproveitar, quer começar às 13h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, aproveitar, começar 13h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é aproveitar, quer começar às 13h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Noite, aproveitar, começar 13h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é aproveitar, quer começar às 13h e passar 5h aproveitando.";
                             echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }else{
                        // Noite, aproveitar e começar 14h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, aproveitar, começar 14h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é aproveitar, quer começar às 14h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, aproveitar, começar 14h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é aproveitar, quer começar às 14h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Noite, aproveitar, começar 14h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é aproveitar, quer começar às 14h e passar 5h aproveitando.";
                             echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }
                }else if($_SESSION['respostas'][1]=='lazer'){
                    // Noite e lazer
                    if($_SESSION['respostas'][2]=='6'){
                        // Noite, lazer e começar 6h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, lazer, começar 6h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é lazer, quer começar às 6h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, lazer, começar 6h e 4h pra prioridade
                            echo"Você estuda à noite, sua prioridade é lazer, quer começar às 6h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Noite, lazer, começar 6h e 5h pra prioridade
                            echo"Você estuda à noite, sua prioridade é lazer, quer começar às 6h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='7'){
                        // Noite, lazer e começar 7h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, lazer, começar 7h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é lazer, quer começar às 7h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, lazer, começar 7h e 4h pra prioridade
                            echo"Você estuda à noite, sua prioridade é lazer, quer começar às 7h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Noite, lazer, começar 7h e 5h pra prioridade
                            echo"Você estuda à noite, sua prioridade é lazer, quer começar às 7h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='13'){
                        // Noite, lazer e começar 13h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, lazer, começar 13h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é lazer, quer começar às 13h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, lazer, começar 13h e 4h pra prioridade
                            echo"Você estuda à noite, sua prioridade é lazer, quer começar às 13h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Noite, lazer, começar 13h e 5h pra prioridade
                            echo"Você estuda à noite, sua prioridade é lazer, quer começar às 13h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }else{
                        // Noite, lazer e começar 14h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, lazer, começar 14h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é lazer, quer começar às 14h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, lazer, começar 14h e 4h pra prioridade
                            echo"Você estuda à noite, sua prioridade é lazer, quer começar às 14h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Noite, lazer, começar 14h e 5h pra prioridade
                            echo"Você estuda à noite, sua prioridade é lazer, quer começar às 14h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }
                }else{
                    // Noite e esbalhar
                    if($_SESSION['respostas'][2]=='6'){
                        // Noite, esbalhar e começar 6h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, esbalhar, começar 6h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é estudar e trabalhar, quer começar às 6h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, esbalhar, começar 6h e 4h pra prioridade
                            echo"Você estuda à noite, sua prioridade é estudar e trabalhar, quer começar às 6h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Noite, esbalhar, começar 6h e 5h pra prioridade
                            echo"Você estuda à noite, sua prioridade é estudar e trabalhar, quer começar às 6h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='7'){
                        // Noite, esbalhar e começar 7h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, esbalhar, começar 7h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é estudar e trabalhar, quer começar às 7h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, esbalhar, começar 7h e 4h pra prioridade
                            echo"Você estuda à noite, sua prioridade é estudar e trabalhar, quer começar às 7h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Noite, esbalhar, começar 7h e 5h pra prioridade
                            echo"Você estuda à noite, sua prioridade é estudar e trabalhar, quer começar às 7h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='13'){
                        // Noite, esbalhar e começar 13h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, esbalhar, começar 13h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é estudar e trabalhar, quer começar às 13h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, esbalhar, começar 13h e 4h pra prioridade
                            echo"Você estuda à noite, sua prioridade é estudar e trabalhar, quer começar às 13h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Noite, esbalhar, começar 13h e 5h pra prioridade
                            echo"Você estuda à noite, sua prioridade é estudar e trabalhar, quer começar às 13h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }else{
                        // Noite, esbalhar e começar 14h
                        if($_SESSION['respostas'][3]=='3'){
                            // Noite, esbalhar, começar 14h e 3h pra prioridade
                            echo"Você estuda à noite, sua prioridade é estudar e trabalhar, quer começar às 14h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Noite, esbalhar, começar 14h e 4h pra prioridade
                            echo"Você estuda à noite, sua prioridade é estudar e trabalhar, quer começar às 14h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Noite, esbalhar, começar 14h e 5h pra prioridade
                            echo"Você estuda à noite, sua prioridade é estudar e trabalhar, quer começar às 14h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }
                }
            }else if($_SESSION['respostas'][0]=='integral'){
                if($_SESSION['respostas'][1]=='vestibular'){
                    // Integral e vestibular
                    if($_SESSION['respostas'][2]=='18'){
                        // Integral, vestibular e começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Integral, vestibular, começar 18h e 3h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é passar no vestibular, quer começar às 18h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Integral, vestibular, começar 18h e 4h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é passar no vestibular, quer começar às 18h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                            ";
                        }else{
                            // Integral, vestibular, começar 18h e 5h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é passar no vestibular, quer começar às 18h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }else{
                        // Integral, vestibular e começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Integral, vestibular, começar 19h e 3h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é passar no vestibular, quer começar às 19h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Integral, vestibular, começar 19h e 4h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é passar no vestibular, quer começar às 19h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                                ";
                        }else{
                            // Integral, vestibular, começar 19h e 5h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é passar no vestibular, quer começar às 19h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }
                }else if($_SESSION['respostas'][1]=='aproveitar'){
                    // Integral e aproveitar
                    if($_SESSION['respostas'][2]=='18'){
                        // Integral, aproveitar e começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Integral, aproveitar, começar 18h e 3h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é aproveitar, quer começar às 18h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";

                        }else if($_SESSION['respostas'][3]=='4'){
                            // Integral, aproveitar, começar 18h e 3h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é aproveitar, quer começar às 18h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Integral, aproveitar, começar 18h e 3h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é aproveitar, quer começar às 18h e passar 5h aproveitando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }else{
                        // Integral, aproveitar e começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Integral, aproveitar, começar 19h e 3h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é aproveitar, quer começar às 19h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Integral, aproveitar, começar 19h e 3h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é aproveitar, quer começar às 19h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Integral, aproveitar, começar 19h e 3h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é aproveitar, quer começar às 19h e passar 5h aproveitando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }
                }else if($_SESSION['respostas'][1]=='lazer'){
                    // Integral e lazer
                    if($_SESSION['respostas'][2]=='18'){
                        // Integral, lazer e começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Integral, lazer, começar 18h e 3h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é lazer, quer começar às 18h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Integral, lazer, começar 18h e 4h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é lazer, quer começar às 18h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Integral, lazer, começar 18h e 5h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é lazer, quer começar às 18h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }else{
                        // Integral, lazer e começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Integral, lazer, começar 19h e 3h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é lazer, quer começar às 19h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Integral, lazer, começar 19h e 4h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é lazer, quer começar às 19h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Integral, lazer, começar 19h e 5h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é lazer, quer começar às 19h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }
                }else{
                    // Integral e esbalhar
                    if($_SESSION['respostas'][2]=='18'){
                        // Integral, esbalhar e começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Integral, esbalhar, começar 18h e 3h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é estudar e trabalhar, quer começar às 18h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Integral, esbalhar, começar 18h e 4h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é estudar e trabalhar, quer começar às 18h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Integral, esbalhar, começar 18h e 5h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é estudar e trabalhar, quer começar às 18h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }else{
                        // Integral, esbalhar e começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Integral, esbalhar, começar 19h e 3h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é estudar e trabalhar, quer começar às 19h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Integral, esbalhar, começar 19h e 4h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é estudar e trabalhar, quer começar às 19h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Integral, esbalhar, começar 19h e 5h pra prioridade
                            echo"Você estuda em período integral, sua prioridade é estudar e trabalhar, quer começar às 19h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }
                }
            }else{
                if($_SESSION['respostas'][1]=='vestibular'){
                    // Trabalha e vestibular
                    if($_SESSION['respostas'][2]=='6'){
                        // Trabalha, vestibular e começar 6h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, vestibular, começar 6h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 6h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, vestibular, começar 6h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 6h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                                ";
                        }else{
                            // Trabalha, vestibular, começar 6h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 6h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='7'){
                        // Trabalha, vestibular e começar 7h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, vestibular, começar 7h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 7h e passar 3h estudando.";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, vestibular, começar 7h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 7h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                                ";
                        }else{
                            // Trabalha, vestibular, começar 7h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 7h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='13'){
                        // Trabalha, vestibular e começar 13h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, vestibular, começar 13h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 13h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, vestibular, começar 13h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 13h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                                ";
                        }else{
                            // Trabalha, vestibular, começar 13h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 13h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='14'){
                        // Trabalha, vestibular e começar 14h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, vestibular, começar 14h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 14h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, vestibular, começar 14h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 14h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                                ";
                        }else{
                            // Trabalha, vestibular, começar 14h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 14h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='18'){
                        // Trabalha, vestibular e começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, vestibular, começar 18h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 18h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, vestibular, começar 18h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 18h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                                ";
                        }else{
                            // Trabalha, vestibular, começar 18h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 18h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }else{
                        // Traalhar, vestibular e começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, vestibular, começar 19h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 19h e passar 3h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos, usando um para estudar e outro para resumir</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para estudos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, vestibular, começar 19h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 19h e passar 4h estudando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos, sendo dois para estudos e os outros dois para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h apenas para estudos!!!</p>
                                ";
                        }else{
                            // Trabalha, vestibular, começar 19h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é passar no vestibular, quer começar às 19h e passar 5h estudando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento dos seus estudos.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias mais importantes e prioritárias</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos, sendo três para estudos e os outros três para resumos</li>
                                    <li>Os ciclos restantes, foque em resolver atividades. Priorizando os assuntos com mais dificuldade</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h apenas para estudos!!!</p>
                            ";
                        }
                    }
                }else if($_SESSION['respostas'][1]=='aproveitar'){
                    // Trabalha e aproveitar
                    if($_SESSION['respostas'][2]=='6'){
                        // Trabalha, aproveitar e começar 6h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, aproveitar, começar 6h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 6h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, aproveitar, começar 6h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 6h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Trabalha, aproveitar, começar 6h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 6h e passar 5h aproveitando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='7'){
                        // Trabalha, aproveitar e começar 7h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, aproveitar, começar 7h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 7h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, aproveitar, começar 7h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 7h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Trabalha, aproveitar, começar 7h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 7h e passar 5h aproveitando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='13'){
                        // Trabalha, aproveitar e começar 13h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, aproveitar, começar 13h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 13h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, aproveitar, começar 13h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 13h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Trabalha, aproveitar, começar 13h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 13h e passar 5h aproveitando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='14'){
                        // Trabalha, aproveitar e começar 14h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, aproveitar, começar 14h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 14h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, aproveitar, começar 14h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 14h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Trabalha, aproveitar, começar 14h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 14h e passar 5h aproveitando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='18'){
                        // Trabalha, aproveitar e começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, aproveitar, começar 18h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 18h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, aproveitar, começar 18h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 18h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Trabalha, aproveitar, começar 18h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 18h e passar 5h aproveitando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }else{
                        // Trabalha, aproveitar e começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, aproveitar, começar 19h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 19h e passar 3h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais pesadas</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h apenas para seus afazeres para que possa aproveitar mais o dia!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, aproveitar, começar 19h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 19h e passar 4h aproveitando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 4h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }else{
                            // Trabalha, aproveitar, começar 19h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é aproveitar, quer começar às 19h e passar 5h aproveitando.";
                            echo "
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as atividades mais importantes e prioritárias para que você diminua seu esforço</li>
                                    <li>Utilize os seis primeiros ciclos de 30 minutos para as atividades mais pesadas, realizando-as com calma</li>
                                    <li>Os ciclos restantes, foque em descansar um pouco, tomar um banho ou escutar música para que esteja totalmente relaxado e pronto para aproveitar o dia</li>
                                </ul>
                                <p style='font-size:15px;'>Como você tem bastante tempo, vá com calma e não faça esforcos desnescessários</p>
                                <p style='font-size:15px;'>A utilização da técnica é opcional, caso não queira usá-la, esteja ciente que você precisa aproveitar essas 5h para se livrar dos afazeres e poder aproveitar o máximo do seu dia!!!</p>
                            ";
                        }
                    }
                }else if($_SESSION['respostas'][1]=='lazer'){
                    // Trabalha e lazer
                    if($_SESSION['respostas'][2]=='6'){
                        // Trabalha, lazer e começar 6h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, lazer, começar 6h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 6h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, lazer, começar 6h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 6h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Trabalha, lazer, começar 6h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 6h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='7'){
                        // Trabalha, lazer e começar 7h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, lazer, começar 7h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 7h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, lazer, começar 7h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 7h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Trabalha, lazer, começar 7h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 7h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='13'){
                        // Trabalha, lazer e começar 13h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, lazer, começar 13h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 13h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, lazer, começar 13h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 13h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Trabalha, lazer, começar 13h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 13h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='14'){
                        // Trabalha, lazer e começar 14h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, lazer, começar 14h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 14h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, lazer, começar 14h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 14h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Trabalha, lazer, começar 14h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 14h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='18'){
                        // Trabalha, lazer e começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, lazer, começar 18h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 18h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, lazer, começar 18h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 18h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Trabalha, lazer, começar 18h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 18h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }else{
                        // Trabalha, lazer e começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, lazer, começar 19h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 19h e passar 3h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os dois primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, lazer, começar 19h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 19h e passar 4h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }else{
                            // Trabalha, lazer, começar 19h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é lazer, quer começar às 19h e passar 5h se divertindo.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Utilize os quatro primeiros ciclos de 30 minutos para as atividades mais energéticas, as que envolvam mais esforço</li>
                                    <li>Use os dois ciclos seguintes para descansarem ou comerem alguma coisa</li>
                                    <li>Os ciclos restantes, foquem em atividades mais leves que não prejudiquem a digestão como jogar games ou jogos de tabuleiros</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 5h para aproveitar o máximo que quiser e da forma que preferir!!!</p>
                            ";
                        }
                    }
                }else{
                    // Trabalha e esbalhar
                    if($_SESSION['respostas'][2]=='6'){
                        // Trabalha, esbalhar e começar 6h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, esbalhar, começar 6h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 6h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, esbalhar, começar 6h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 6h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Trabalha, esbalhar, começar 6h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 6h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='7'){
                        // Trabalha, esbalhar e começar 7h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, esbalhar, começar 7h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 7h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, esbalhar, começar 7h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 7h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Trabalha, esbalhar, começar 7h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 7h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='13'){
                        // Trabalha, esbalhar e começar 13h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, esbalhar, começar 13h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 13h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, esbalhar, começar 13h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 13h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Trabalha, esbalhar, começar 13h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 13h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='14'){
                        // Trabalha, esbalhar e começar 14h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, esbalhar, começar 14h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 14h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, esbalhar, começar 14h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 14h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Trabalha, esbalhar, começar 14h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 14h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }else if($_SESSION['respostas'][2]=='18'){
                        // Trabalha, esbalhar e começar 18h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, esbalhar, começar 18h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 18h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, esbalhar, começar 18h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 18h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Trabalha, esbalhar, começar 18h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 18h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }else{
                        // Trabalha, esbalhar e começar 19h
                        if($_SESSION['respostas'][3]=='3'){
                            // Trabalha, esbalhar, começar 19h e 3h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 19h e passar 3h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Devido ao curto tempo. recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os três primeiros ciclos para seus estudos e os outros três para seu trabalho</li>
                                    <li>Como você não tem tanto tempo, escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 3h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else if($_SESSION['respostas'][3]=='4'){
                            // Trabalha, esbalhar, começar 19h e 4h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 19h e passar 4h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os quatro primeiros ciclos para seus estudos e os outros quatro para seu trabalho</li>
                                    <li>Escolha as atividades mais urgentes no momento</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }else{
                            // Trabalha, esbalhar, começar 19h e 5h pra prioridade
                            echo"Você trabalha, sua prioridade é estudar e trabalhar, quer começar às 19h e passar 5h estudando e trabalhando.";
                            echo"
                                <p style='font-size: 20px;'>Recomendamos que utilize a técnica do Pomodoro para melhor aproveitamento das suas atividades.</p>
                                <p style='font-size: 15px;'><strong>Algumas dicas de como organizar sua tabela utilizando a técnica do Pomodoro:</strong></p>
                                <ul style='list-style: square;'>
                                    <li>Separe as matérias que mais precisam de atenção, assim como alguma atividade do seu trabalho</li>
                                    <li>Use os cinco primeiros ciclos para seus estudos e os outros cinco para seu trabalho</li>
                                    <li>Como você tem um certo tempo, vá com calma e respeite seus tempos de descanso</li>
                                </ul>
                                <p style='font-size:15px;'>Caso não queira utilizar a técnica do Pomodoro, esteja ciente que você precisa aproveitar essas poucas 4h para realizar o máximo de atividades dos seus estudos e trabalhos!!!</p>
                            ";
                        }
                    }
                }
            }        
            // Elementos fixos(não mudam conforme o resultado)
            echo "
            <br><br>
            </div>";
            echo"
            
                <form action='../controle/redirect.php' method='post'>
                    <input type='hidden' name='n0w3' value='{$_GET["n0w3"]}' />
                    <input style='float: right; padding: 8px 9.5px; background-color: #00ccff;' class='btn' type='submit' name='inicio' value='Recomeçar' />
                </form> 
            ";
        
    }
}
echo"
    </div>
    <script src='js/cor.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js' integrity='sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl'
    crossorigin='anonymous'></script>
</body>
</html>
";
}else{
    header("Location: Login.php");
}
?>
