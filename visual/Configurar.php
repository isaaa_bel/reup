<?php
session_start();
if(!isset($_SESSION['log'])){
    $_SESSION['log']=true;
}
if(isset($_SESSION['sessao'])){
require_once("../controle/UsuarioControle.php");
require_once("../modelo/Usuario.php");
$controle=new ControleUsuario();
$item=$controle->selecionarTodos();
if($item!=null){
    foreach($item as $atual){
        $hash=md5($atual['email']);
        if($hash==$_GET['n0w3']){
            $user=new Usuario();
            $user->setNome($atual['nome']);
            $user->setSenha($atual['senha']);
            $user->setEmail($atual['email']);
            break;
        }
    }
}
echo "
<html>
<head>
    <title>Configurações</title>
    <meta charset='utf-8'>
    <link rel='stylesheet' href='menu.css' >
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' href='css/bootstrap.css' crossorigin='anonymous'>
    <link rel='icon' type='imagem/png' href='imagem/Logo.svg.png'>
    <style>
        @media(max-width: 500px){
            .conf{
                width: 300px;
                margin-right: 20px;
                margin-top: -10px;
            }
            #alteraSenha{
                width: 300px;
                margin-left: 10px;  
            }
            #alteraNome{
                width: 300px;   
            }
        }
        #alteraNome{
            margin-left: 10px;   
        }
        #cor{
            color: #000;
        }
        #ba{
            background-color: #61c8ce;
        }
        #gif{
            align: center;
            width: 100px;
            height: 100px;
            display: flex;
        }

        .btn{
            background-color: #61c8ce;
            color: black;
        }
        .btn:hover{
            color: white;
        }
        .del{
            height: 40px;
            background-color: red;
            color: white;
            border-radius: 12px;
        }
        .del:hover{
            color: black;
            border-radius: 8px;
        }
        #form{
            display: inline;
            margin-left: 10% !important;
        }
        #arra{
            margin-left: 10%;
        }
    </style>
</head>
<body>
    <nav class='navbar navbar-expand-lg navbar-light' id='ba'>
    <a class='navbar-brand' href='../index.php' id='cor'><img src='imagem/Logo.svg.png' width='40'>eUp</a>
    <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarNavDropdown' aria-controls='navbarNavDropdown' aria-expanded='false' aria-label='Toggle navigation'>
    <span class='navbar-toggler-icon'></span>
    </button>
    <div class='collapse navbar-collapse' id='navbarNavDropdown'>
    <ul class='navbar-nav'>
        <li class='nav-item'>
        <a class='nav-link' id='cor' href='Principal.php?n0w3={$_GET["n0w3"]}'>Projetos</a>
        </li>
        <li class='nav-item'>
        <a class='nav-link' id='cor' href='Quiz.php?n0w3={$_GET["n0w3"]}'>Quiz</a>
        </li>
        <li class='nav-item dropdown'>
        <a class='nav-link dropdown-toggle' id='cor' href='#' id='navbarDropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            Mais
        </a>
        <div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
            <a id='cor' class='dropdown-item' href='Ajuda.php?n0w3={$_GET["n0w3"]}'>Ajuda</a>
            <a class='dropdown-item' href='../controle/sair.php'>Sair</a>
        </div>

        </li>
    </ul>
    </div>
    </nav>

        <div class='container'>
        <div class='float-right'>
            <img  class='conf' src='../visual/imagem/btn.jpeg'>
        </div>
        <br>
            <div class='img'><center><img src='../visual/imagem/engrenagem.gif' id='gif'></div><br>
            <div class='present'><h2 align='center'>Faça Alterações na sua conta !<h2></div><br>
                    <div class='form'>
                        <form action='../controle/Alteracoes.php' method='post' id='arra'>
                            <input type='hidden' name='oldEmail' value='{$user->getEmail()}' />
                            <div class='row'>
                                <label for='alteraNome'><strong>Seu Nome:</strong></label>
                                <div class='col md-6'>
                                    <input type='text' class='form-control' name='nome' id='alteraNome' value='{$user->getNome()}'>
                                </div>
                                <div class='col'>
                                    
                                </div>
                            </div>
                            <br><br>
                            <div class='row'>
                                <label for='alteraSenha'><strong>Nova Senha:</strong></label>
                                <div class='col md-6'>
                                    <input type='password' title='Sua senha salva é: {$user->getSenha()}' class='form-control' name='senha'  id='alteraSenha' value={$user->getSenha()}>
                                </div>
                                <div class='col'>
                                    
                                </div>
                            </div>
                            <br>
                             <div class='col'>
                                    <input type='submit' class='btn'value='Editar'>
                                </div>
                            <br>
                            <p>Caso queira alterar suas informações, basta inserí-las e clicar em editar!</p>
                        </form>
                    </div>
                            <p><strong>Caso queira deletar sua conta, clique no botão abaixo!</strong></p>
                            <input type='hidden' name='emailDel' id='emailDel' value='{$user->getEmail()}' />
                            <input type='submit' id='clickDel' class='del' value='Deletar conta' />

        </div>
         <br /> <br />
</body>
    <script src='js/sweet.js'></script>
    <script src='js/deletar.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js' integrity='sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl'
    crossorigin='anonymous'></script>
</html>
";
if(isset($_SESSION['erro'])){
    if($_SESSION['erro']){
        echo"<script>swal('Ops','Não foi possível realizar as alterações, esse email já foi cadastrado','error');</script>";
    }else{
        echo"<script>swal('OK','As alterações foram realizadas com sucesso','success');</script>";
    }
    unset($_SESSION['erro']);
}
if(isset($_SESSION['erroDel'])){
    echo"<script>swal('Ops','Desculpe, Não foi possível deletar sua conta','error')</script>";
    unset($_SESSION['erroDel']);
}
}else{
    header("Location: Login.php");
}
?>
