<?php
session_start();
if(isset($_SESSION['adm'])){
    require_once("../controle/ComentarioControle.php");
    $controle=new ComentarioControle();
    $item=$controle->selecionarTodos();
    echo "
    <!DOCTYPE html>
	<html lang='pt-br'>

	<head>
        <title>Comentários</title>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <link rel='stylesheet' href='css/bootstrap.css' crossorigin='anonymous'>
        <link rel='icon' type='imagem/png' href='imagem/Logo.svg.png'>
        <style> 
            body{
                background-color: #45eaff;
            }   
            .sa{
                color: #000;
                float: right;                
            }
            .vertical-align {
                margin: 0;    
                position: absolute;
                top: 50%;
                left: 50%;
                margin-right: -50%;
                transform: translate(-50%, -50%);   
              }
              
        </style>    
    </head>
    <body>
    <div class='container'>
    ";
    if($item!=null){
        foreach($item as $atual){
            $ultimo=$atual['id'];
            $id=$ultimo;
            $cont=1;
        }
        echo"
        
        <div class='my-3 p-3 bg-white rounded shadow-sm '>
        <br>
        <div class='table-responsive'>
        <table class='table'>
                <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Id</th>
                            <th scope='col'>Email</th>
                            <th scope='col'>Comentário</th> 
                            <th scope='col'>Responder</th>
                            <th scope='col'>Deletar</th>                                                                                    
                        </tr>
                    </thead>
        ";        
        for($i=0;$i<$ultimo;$i++){
            foreach($item as $atual){
                if($atual['id']==$id){
                    $idH=md5($atual['id']);
                    echo"
                    <tbody>
                        <tr>
                        <th scope='row'>{$cont}</th>
                        <td>{$atual['email']}</td>
                        <td>{$atual['comentario']}</td>
                        <td><a target='_blanck' href='https://mail.google.com/mail/u/1/#inbox?compose=new'>responder</a></td>
                        <td><a href='confDelCom.php?id={$idH}'>Deletar</a></td>
                        </tr>
                    </tbody>

                    ";
                    $cont++;
                    $id--;
                }
            }
        }
        echo"</table>
        </div   
        </div>
        ";    
    }else{
        echo"<h3>Não há comentários.</h3>";
    }
    echo"<a href='../controle/sair.php' class='sa'>Sair</a><br><br>";    
    echo "
    </div>
    <script src='js/sweet.js'></script>
    </body>
    </html>
";
    if(isset($_SESSION['erro'])){
        if($_SESSION['erro']){
            echo"<script>swal('Ops','Não foi possível apagar o comentário','error');</script>";
        }else{
            echo"<script>swal('OK','O comentário foi apagado com sucesso','success');</script>";
        }
        unset($_SESSION['erro']);
    }
}else{
    header("Location: ../index.php");
}
?>
