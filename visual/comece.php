<?php

echo "
<html>
<head>
    <title>Começando | ReUp</title>
	<meta charset='utf-8'>
	<meta name='viewport' content='width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no'>
    <link rel='stylesheet' href='css/bootstrap.css' >	
	<link rel='icon' type='imagem/png' href='imagem/Logo.svg.png'>

<style>  
body{
	background-color: #bbccf2;
} 

.vertical-align {
	margin: 0;    
	position: absolute;
	top: 50%;
	left: 50%;
	margin-right: -50%;
	transform: translate(-50%, -50%);   
  }

button{
	border-style: none;
} 

a:hover{
	text-decoration: none !important;
}

#c{
	margin-top: 5%;	
	animation-name: cN;
	animation-duration: 3s;
	text-align: center;
}

#o{
	animation-name: oN;
	animation-duration: 6s;
	text-align: center;
}

#i{
	margin-bottom: 5%;
	animation-name: iN;
	animation-duration: 7s;
	text-align: center;	
}

#cor{
	color: #000;
}

#n{
	margin-left: 10%;
	width: 100px;
	height: 200px;
	border-radius: 10px;
	background-color: #8fb3f2;
	padding: 50px;
	margin-bottom: 0%;
	animation-name: nN;
	animation-duration: 2s;
	display: inline;
}

#t{
	display: inline;
	margin-left: 10%;	
	width: 100px;
	height: 200px;
	border-radius: 10px;	
	padding: 50px;
	background-color: #8fb3f2;
	animation-name: tN;
	animation-duration: 0.2s;
}

@keyframes cN{
	from{
		color: #bbccf2; 	
	}
	to{color: #000; }
}

@keyframes oN{
	from{
		color: #bbccf2; 	
	}
	to{color: #000; }
}

@keyframes iN{
	from{
		color: #bbccf2; 	
	}
	to{color: #000; }
}

@keyframes nN{
	from{
		margin-top: 0%;
		margin-left: -20%;
	}
	to{margin-left: 10%;
	}
}

#f{
	background-color: #8fb3f2;
}

@media(max-width: 900px){
	#n{
		background-color: #8fb3f2;
		margin-left: 10%;
		margin-top: 10%;
		display: block;	
		margin-bottom: 20%;
		width: 350px;	
		border-radius: 10px;		
		animation-name: nN;
		animation-duration: 2s;
		margin: 0;    
		position: absolute;
		top: 50%;
		left: 50%;
		margin-right: -50%;
		transform: translate(-50%, -50%);   
			
	}
	#t{
		background-color: #8fb3f2;
		margin-left: 10%;	
		display: block;		
		width: 350px;
		height: 200px;
		border-radius: 10px;					
		animation-name: tN;
		animation-duration: .2s;
		margin: 0;    
		position: absolute;
		top: 80%;
		left: 50%;
		margin-right: -50%;
		transform: translate(-50%, -50%);   
	}
	@keyframes nN{
		from{
			margin-top: -200%;
		}
		to{
			
		}
	}
	
}

@media(max-width: 450px){
	#n{
		background-color: #8fb3f2;
		margin-left: 10%;
		margin-top: 20%;
		display: block;	
		margin-bottom: 20%;
		width: 300px;	
		border-radius: 10px;		
		animation-name: nN;
		animation-duration: 2s;
		margin: 0;    
		position: absolute;
		top:80%;
		left: 50%;
		margin-right: -50%;
		transform: translate(-50%, -50%);   
			
	}
	#t{
		background-color: #8fb3f2;
		margin-left: 10%;	
		display: block;		
		width: 300px;
		height: 200px;
		border-radius: 10px;					
		animation-name: tN;
		animation-duration: .2s;
		margin: 0;    
		position: absolute;
		top: 50%;
		left: 50%;
		margin-right: -50%;
		transform: translate(-50%, -50%);   
	}
	@keyframes nN{
		from{
			margin-top: -200%;
		}
		to{
			
		}
	}

	
}

</style>    
</head>

<body>
    <h1 id='c'>Seja Bem Vindo</h1>
    <h2 id='o'>Para começar</h2>
    <h3 id='i'>Como você prefere prosseguir?</h3>
    <div class='container'>
    	<div class='row'>
    		<div id='n' class='col-md-4 mb-3' >			    
			    <a id='cor' href='Quiz.php?n0w3={$_GET["n0w3"]}'>
			    	<button id='f'>
			    		<h5 >Realizar quiz, para que criemos a sua organização!</h5>
			    	</button>
			    </a>
			</div>
			<div id='t'  class='col-md-4' >			    
			    <a id='cor' href='Principal.php?n0w3={$_GET["n0w3"]}'>
			    	<button id='f'>
			    		<h5>Clique aqui para realizar sua própria organização!</h5>
			    	</button>
			    </a>
			</div>		    
		</div>		    
    <div>
</body>
<script src='js/sweet.js'></script>

</html>
";
?>