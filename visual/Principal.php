<?php
session_start();
if(isset($_SESSION['sessao'])){
require_once("../modelo/CalendarioModelo.php");
require_once("../controle/CalendarioControle.php");
require_once("../controle/UsuarioControle.php");
require_once("../controle/TabelaUserControle.php");
require_once("../controle/ProjetoControle.php");
$tabela=new TabelaUser();
$projeto=new Projeto();
$ctrlUser=new ControleUsuario();
$controleT=new TabelaUserControle();
$controleP=new ProjetoControle();
$item=$ctrlUser->selecionarTodos();
$controle = new CalendarioControle();
$calendario = $controle->selecionar();
if($item!=null){
    foreach($item as $atual){
        $n=md5($atual['email']);
        if($n==$_GET['n0w3']){
            $n=$atual['nome'];
            $e=$atual['email'];
            $tabela->setUsuario($atual['email']);
            break;
        }
    }
}
//Pegar dados da tabela com o email do user
//Pegar os ids dos projetos
//Armazenar os ids dos projetos em um array
$item=$controleT->selecionarPorUser($tabela);
if($item!=null){
    $idP=Array();
    $cont=0;
    foreach($item as $atual){
        array_push($idP,$atual['projeto']);
        break;
    }
    foreach($item as $atual){
        if($idP[0]!=$atual['projeto']){
            array_push($idP,$atual['projeto']);
            $cont++;
            if($idP[$cont]==$idP[$cont-1]){
                array_pop($idP);
                $cont--;
            }
        }
    }
    $nomeP=Array();
    for($i=0;$i<sizeOf($idP);$i++){
        $projeto->setId($idP[$i]);
        $item=$controleP->selecionarUm($projeto);
        foreach($item as $atual){
            array_push($nomeP,$atual['nome']);
        }
    }
}
echo '
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Organizando a tabela</title>
    <meta charset="utf-8">    
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <meta name="description" content="Responsive sidebar template with sliding effect and dropdown menu based on bootstrap 3">
    <link rel="stylesheet" href="css/menuV.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">    
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="css/menu.css" >
    <link rel="icon" type="imagem/png" href="imagem/Logo.svg.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>

<div class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm co se" href="#">
    <i class="fas fa-bars a"></i>
  </a>
  <nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
      <div class="sidebar-brand">
        <a href="#">Menu</a>
        <div id="close-sidebar">
          <i id="id" class="fas fa-times"></i>
        </div>
      </div>
      <div class="sidebar-header">
        <div class="user-info">
          <span class="user-name">';
            if(!isset($_SESSION['log'])){
                echo"Bem vindo ";
            }
            echo"<a href='Configurar.php?n0w3={$_GET["n0w3"]}'><strong class='text-light'>{$n}</strong></a>";
            echo'
          </span>
          

        </div>
      </div>
      <!-- sidebar-header  -->
      
      <div class="sidebar-menu">
        <ul>
          <li class="header-menu">
            <span>Geral</span>
          </li>
          <li class="sidebar-dropdown">';
            echo"<a href='#'>";
            echo'
              <i class="fa fa-folder"></i>
            <span>Meus projetos</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
';
                if(!isset($idP)){
                    echo"
                    <li class='text-light'>Você não possui projetos</li>
                    ";
                }else{
                    for($i=0;$i<sizeOf($idP);$i++){
                        echo"<li>
                                <a target='_blank' href='Projetos.php?n0w3={$_GET['n0w3']}&id={$idP[$i]}'>{$nomeP[$i]}</a>
                            </li>";
                    }
                }
                  echo '
              </ul>
            </div>
          </li>
          <li class="sidebar">';
            echo"<a href='Quiz.php?n0w3={$_GET["n0w3"]}'>";
            echo'
              <i class="far fa-gem"></i>
              <span>Quiz</span>
            </a>
          </li>

          <li class="header-menu">
            <span>Extra</span>
          </li>
          <li>';
        echo "<a href='Lembrete.php?n0w3={$_GET["n0w3"]}'>";
        echo '<i class="fa fa-calendar"></i>
              <span>Lembretes</span>
            </a>
          </li>

          <li>';
            echo"<a title='Você poderá usar essa página para ter seu tempo adequado para estudo com a técnica do pomodoro.' target='_blanck' href='Pomodoro.php?n0w3={$_GET["n0w3"]}'>";
            echo'
              <i class="fa fa-tachometer-alt"></i>
              <span>Técnica do pomodoro</span>
            </a>            
          </li>

        </ul>
      </div>
      <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-content  -->
    <div class="sidebar-footer">';
    echo "<a href='Nots.php?n0w3={$_GET["n0w3"]}'>";
    echo "<i class='fa fa-bell'></i>";
$contador = 0;
foreach ($calendario as $value){
  $idH=md5($value->getId());
  $email=md5($value->getUsuario());
  if($email==$_GET['n0w3']){
    date_default_timezone_set('UTC');
    if(date('Y-m-d') == $value->getDataa() || date('Y-m-d') > $value->getDataa()){
      if($value->getConcluir() == "false"){
        $contador++;
      }
    }
  }
}
if($contador != 0){
    echo "
      <span style='background-color: #71e8f5; color: #000' class='badge badge-pill badge-warning notification'>{$contador}</span>
    </a>";
  }
    echo"<a href='Configurar.php?n0w3={$_GET["n0w3"]}'>";
    echo'
        <i class="fa fa-cog"></i>
      </a>
      <a href="../controle/sair.php">
        <i class="fa fa-power-off"></i>
      </a>
    </div>
  </nav>

  <!-- sidebar-wrapper  -->
  <main class="page-content">
    <div class="container-fluid">

    <!--Imagem de ajuda-->
      ';
      echo "
      
      <button id='te' type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#exampleModalLong'>Criar Projeto</button>      

      <div class='modal fade' id='exampleModalLong' tabindex='-1' role='dialog' aria-labelledby='exampleModalLongTitle' aria-hidden='true'>
        <div class='modal-dialog' role='document'>
          <div class='modal-content'>
            <div class='modal-header'>
              <h4 class='modal-title' id='exampleModalLongTitle'> Meu projeto</h4>
              <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>
            <div class='modal-body'>
            <form action='../controle/addProject.php' method='post'>
                <label for='nome'>Nome do Projeto: </label>
                <br>
                <input type='hidden' name='n0w3' value='{$_GET["n0w3"]}' />
                <input type='text' name='nome' id='nome' max-length='50' required/>
                <br><br>
                <label for='desc'>Descrição: </label>
                <br>
                <textarea cols='25' rows='5' id='desc' type='text' name='descricao' max-length='200' required></textarea>                
                <br><br>
                <input type='submit' class='btn btn-info' value='Criar' />
          </form>
            </div>
            <div class='modal-footer'>
              <button type='button' class='btn btn-secondary' data-dismiss='modal'>Fechar</button>              
            </div>
          </div>
        </div>
      </div>  
      <a title='Ajuda' href='Ajuda.php?n0w3={$_GET["n0w3"]}'>
          <img src='imagem/aju.png' id='te' width='48'>
        </a>        

        <h3>Crie seu projeto!</h3>
        <hr>
        <br><br>
        <div class='container'>                       
              <div class='d-flex align-items-center p-3 my-3 text-white-50 rounded shadow-sm' id='aa'>
                  <img src='imagem/folder.png' id='he'>                    
              <div class='lh-100' id='fica'>
                  <h3 class='mb-0 text-light lh-100'>Como Alterar o projeto</h3>          
              </div>
              </div>
              <div class='row'>
                <div class='col-md-9 mb-3'>                                                          
                  <h4>Clique em criar projeto e dê um nome e uma descrição para seu projeto
                  <br>
                  Você poderá adicionar quantas linhas quiser, apertando no botão que está circulado de azul.</h4>    
                </div>
              </div>
              <div class='row'>
                <div class='col-md-9 mb3'>
                <h4>
                Depois é só colocar o horário e o que você deseja fazer! Simples e pático
                <br>
                Veja o projeto abaixo como exemplo:
                </h4>    
                </div>
              </div>  
              <div>
                <img class='featurette-image img-fluid mx-auto' src='imagem/Foto.png.svg' alt='Generic placeholder image'>
              </div>                                                        
        </div>                          
        ";                
        echo '
     
<!-- page-wrapper -->        
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.min.js"></script>                
<script src="js/a.js"></script>
<script src="js/c.js"></script>
<script src="js/sweet.js"></script>
</html>
';
if(isset($_SESSION['erroProjeto'])){
    if($_SESSION['erroProjeto']){
        echo"<script>swal('Ops','Não foi possível criar o projeto, já existe um projeto seu com esse nome','error');</script>";
    }else{
        echo"<script>swal('OK','Projeto criado com sucesso','success');</script>";
    }
    unset($_SESSION['erroProjeto']);
}
if(isset($_SESSION['erroDelProjeto'])){
    if(!$_SESION['erroDelProjeto']){
        echo"<script>swal('Projeto apagado','Seu projeto foi apagado do sistema','info');</script>";
    }
    unset($_SESSION['erroDelProjeto']);
}
}else{
    header("Location: Login.php");
}
?>
