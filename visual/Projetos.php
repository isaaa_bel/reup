<?php
session_start();
if(isset($_SESSION['sessao'])){
require_once("../controle/UsuarioControle.php");
require_once("../controle/TabelaUserControle.php");
require_once("../controle/ProjetoControle.php");
$projeto=new Projeto();
$tabela=new TabelaUser();
$controleT=new TabelaUserControle();
$controleP=new ProjetoControle();
$ctrlUser=new ControleUsuario();
$projeto->setId($_GET['id']);
$item=$ctrlUser->selecionarTodos();
if($item!=null){
    foreach($item as $atual){
        $n=md5($atual['email']);
        if($n==$_GET['n0w3']){
            $n=$atual['nome'];
            $e=$atual['email'];
            $tabela->setUsuario($atual['email']);
            $tabela->setProjeto($_GET['id']);
            break;
        }
    }
}
$item=$controleP->selecionarUm($projeto);
foreach($item as $atual){
    $projeto->setNome($atual['nome']);
    $projeto->setDescricao($atual['descricao']);
}
echo '
<!DOCTYPE html>
<html lang="pt-br">

<head>';
    echo "<title>{$projeto->getNome()}</title>";
    echo '
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <link rel="stylesheet" href="css/menuV.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">    
    <link rel="stylesheet" href="css/menu.css" >
    <link rel="icon" type="imagem/png" href="imagem/Logo.svg.png">
</head>
<body>';
echo"

<nav class='navbar navbar-expand-lg navbar-dark' style='background-color: #212529;'>
    <a class='navbar-brand' href='../index.php' id='cor'><img src='imagem/Logo.svg.png' width='40'>eUp</a>                
</nav>
";


echo'
<div class="page-wrapper chiller-theme">
  <!-- sidebar-wrapper  -->
  <main class="page-content">
    <div class="container-fluid">

    <!--Imagem de ajuda-->
      ';        
        echo "
            <h2>{$projeto->getNome()}</h2>
        ";
        echo'<hr>
        <div class="row">
        <div class="form-group col-md-12">';
        echo"
            <p>{$projeto->getDescricao()}</p>
        ";
            echo"<button value='{$e}' class='{$_GET["id"]}' title='Clique para adicionar linha' id='addLinha'><a href='#' id='s'>+</a></button>
        </div>
";
echo'
<div class="table-responsive">
<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">Hora</th>
            <th scope="col">Segunda</th>
            <th scope="col">Terça</th>
            <th scope="col">Quarta</th>
            <th scope="col">Quinta</th>
            <th scope="col">Sexta</th>
            <th scope="col">Deletar</th>
        </tr>
    </thead>
';
$item=$controleT->selecionarAvancado($tabela);
echo"<tbody>";
foreach($item as $atual){
    echo"<tr>";
        if($atual['hora']!=""){
        echo"<td class='hora' scope='row' title='{$atual["id"]}'>{$atual['hora']}</td>";
    }else{
        echo"<td class='hora' scope='row' title='{$atual["id"]}'>(Clique para adicionar)</td>";
    }
    if($atual['segunda']!=""){
        echo"<td class='segunda' title='{$atual["id"]}'>{$atual['segunda']}</td>";
    }else{
        echo"<td class='segunda' title='{$atual["id"]}'>Vazio</td>";
    }
    if($atual['terca']!=""){
        echo"<td class='terca' title='{$atual["id"]}'>{$atual['terca']}</td>";
    }else{
        echo"<td class='terca' title='{$atual["id"]}'>Vazio</td>";
    }
    if($atual['quarta']!=""){
        echo"<td class='quarta' title='{$atual["id"]}'>{$atual['quarta']}</td>";
    }else{
        echo"<td class='quarta' title='{$atual["id"]}'>Vazio</td>";
    }
    if($atual['quinta']!=""){
        echo"<td class='quinta' title='{$atual["id"]}'>{$atual['quinta']}</td>";
    }else{
        echo"<td class='quinta' title='{$atual["id"]}'>Vazio</td>";
    }
    if($atual['sexta']!=""){
        echo"<td class='sexta' title='{$atual["id"]}'>{$atual['sexta']}</td>";
    }else{
        echo"<td class='sexta' title='{$atual["id"]}'>Vazio</td>";
    }
    echo"
    <td><a href='confDelLinha.php?id={$atual["id"]}&n0w3={$_GET["n0w3"]}&p={$_GET["id"]}'><img src='imagem/del.png'></a></td>
    </tr>";
}
echo"</tbody>";
echo"</table>";
echo'
<hr>
      </div>
  <!-- page-content" -->
</div>';
echo "
<br>
<button id='delP' type='button' title='{$_GET["id"]}' value='{$_GET["n0w3"]}' class='btn btn-danger'>Deletar Projeto</button>";
echo'
</div>
<!-- page-wrapper -->
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
        <script src="js/sweet.js"></script>
        <script src="js/c.js"></script>
        <script src="js/b.js"></script>
</body>
</html>
';
if(isset($_SESSION['erroUp'])){
    if($_SESSION['erroUp']){
        echo"<scrip>swal('Ops','Não foi possível alterar o valor do campo','error');</script>";
    }
    unset($_SESSION['erroUp']);
}
if(isset($_SESSION['erroIn'])){
    if($_SESSION['erroIn']){
        echo"<scrip>swal('Ops','Não foi possível criar uma linha na tabela','error');</script>";
    }else{
        echo"<script>swal('OK','Linha adicionada','success');</script>";
    }
    unset($_SESSION['erroIn']);
}
if(isset($_SESSION['erroDelLinha'])){
    if($_SESSION['erroDelLinha']){
        echo"<script>swal('Ops','Não foi possível deletar a linha','error');</script>";
    }else{
        echo"<script>swal('OK','Linha deletada','success');</script>";
    }
    unset($_SESSION['erroDelLinha']);
}
if(isset($_SESSION['erroDelProjeto'])){
    if($_SESSION['erroDelProjeto']){
        echo"<script>swal('Ops','Não foi possível deletar este projeto','error');</script>";
    }
    unset($_SESSION['erroDelProjeto']);
}
}else{
    header("Location: Login.php");
}
?>
