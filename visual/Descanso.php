<?php
echo "
<html>
<head>
    <title>Descanso</title>
    <meta charset='utf-8'>
    <link rel='stylesheet' href='menu.css' >
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' href='css/bootstrap.css' crossorigin='anonymous'>
    <link rel='stylesheet' href='css/geral.css'>
    <link rel='icon' type='imagem/png' href='imagem/Logo.svg.png'>
    <style>
        .corpo{
            background-color: afeeee !important;
        }
        #cor{
            color: #000;
        }
        #ba{
            background-color: #2edaf0;
        }
        .container{
            background-color: white;
            border-radius: 7px;
            -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
        }
        .timer{
            color: #000080;
            font-size: 150px;
            text-align: center;
            font-family: verdana;
            font-style: oblique;
        }
        .apr{
            font-size:55px;
            font-family:impact;
            text-align:center;
            text-shadow: 3px -2px 2px #cdc9c9;
        }
        .dicas{
            list-style: square inside;
            font-size: 25px;
        }
    </style>
</head>
<body class='corpo' id='teste'>    
    <br><br><br>
    <div class='container'>
    <br>
        <div>
            <h1 class='apr'>Hora de descansar um pouco!</h1>
        </div>
        <p class='timer'>
            <span id='timer_minutos'>05</span> :
            <span id='timer_segundos'>00</span>
        </p>
        <div>
            <p style='font-size:30px;font-family:verdana;'><strong>Algumas sugestões de relaxar e descansar sua mente:</strong></p>
            <ul class='dicas'>
                <li>Escutar uma música que lhe acalme</li>
                <li>Tomar um ar e beber um pouco de água</li>
                <li>Relaxar os músculos para evitar fadiga</li>
            </ul>
            <br>
        </div>
        <div>
            <p style='font-size:20px; text-align:center;'>Após passarem os 5 minutos, você será redirecionado de volta para a página de estudos. Aproveite seu descanso!</p>
        </div>
        <br>
        <input type='hidden' value='{$_GET['n0w3']}' id='n0w3' />
    </div>
    <script type='text/javascript'>
        function Contador(m_init){
            var t_minutos = document.getElementById('timer_minutos');
            var t_segundos = document.getElementById('timer_segundos');
            t_minutos.innerHTML = ((m_init - 1) > 9) ? ('' + (m_init - 1)) : ('0' + (m_init - 1));
            t_segundos.innerHTML = '59';
            var m = m_init - 1;
            var s = '59';
            var count = setInterval(function(){
                t_minutos.innerHTML = (m > 9) ? ('' + m) : ('0' + m);
                t_segundos.innerHTML = (s > 9) ? ('' + s) : ('0' + s);
                if(s > 0){
                    s -= 1;
                }else if(s == 0 && m > 0){
                    s = 59;
                    m -= 1;
                }else{
                    m = m_init;
                }
            }, 1000);
        }
        Contador(5);
    </script>
    </body>
    <script src='js/sweet.js'></script>
    <script src='js/volta.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js' integrity='sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl'
crossorigin='anonymous'></script>
</html>";
?>
