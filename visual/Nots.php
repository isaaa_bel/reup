<?php
session_start();
if(isset($_SESSION['sessao'])){
require_once("../modelo/CalendarioModelo.php");
require_once("../controle/UsuarioControle.php");
require_once("../controle/CalendarioControle.php");
$controle = new CalendarioControle();
$controleUser=new ControleUsuario();
$item=$controleUser->selecionarTodos();
$calendario = $controle->selecionar();
foreach($item as $atual){
    $mail=md5($atual['email']);
    if($mail==$_GET['n0w3']){
        $mail=$atual['email'];
        break;
    }
}
if(!isset($_SESSION['log'])){
    $_SESSION['log']=true;
}	
	echo '
	<!DOCTYPE html>
	<html lang="pt-br">

	<head>
	<title>Notificações</title>
	<meta charset="utf-8">	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrapi.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> 		
	<link rel="icon" type="imagem/png" href="imagem/Logo.svg.png">
	</head>
<style>
body{
	background-color: #a1cdff;	
}
#R{
	background-color: #0E73AA;	
	margin-top: 10% !important;
}

.notf{
   width: 300px; 
}
.a{
	float: right;
}
@media(max-width: 667px){
    #R{
        background-color: #0E73AA;
        width: 320px;
    }
    .notf{
       width: 300px; 
    }
    .a{
        margin-left: 100px;

    }
}
</style>

	<body>    
	<main role="main" class="container">
		<div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded shadow-sm" id="R">
			<img class="mr-3" src="imagem/Logo.svg.png"  width="48" height="48">
			<div class="lh-100">
				<h1 class="mb-0  text-white lh-100" id="h6"> Notificações</h1>
			</div>
        </div>

		<div class="my-3 p-3 bg-white rounded shadow-sm">
		<h6 class="border-bottom border-gray pb-2 mb-0">Notificações do Dia</h6>
		<br />
';
foreach ($calendario as $value){
	$idH=md5($value->getId());
    $email=md5($value->getUsuario());
    if($email==$_GET['n0w3']){
    	date_default_timezone_set('UTC');
		if(date('Y-m-d') == $value->getDataa()){
			if($value->getConcluir() == "false"){
				echo"<h6 class='border-bottom h5' id='dia' ><span style='font-weight: bold'>Chegou seu dia! Não esqueça de conclui-lo: </span> <a href='Calendario.php?n0w3={$_GET["n0w3"]}'>{$value->getAssunto()}</a></h6>";
			}
		}
	}
}
echo"
	</div>
	<div class='my-3 p-3 bg-white rounded shadow-sm'>
	<h6 class='border-bottom border-gray pb-2 mb-0'>Notificações Pendentes</h6>
	<br/>
";
foreach ($calendario as $value){
	$idH=md5($value->getId());
    $email=md5($value->getUsuario());
    if($email==$_GET['n0w3']){
		date_default_timezone_set('UTC');
		if(date('Y-m-d') >= $value->getDataa()){
			if($value->getConcluir() == "false"){
				echo "      
					<h6 class='border-bottom h5' ><span style='font-weight: bold'>Você deve concluir o: </span> <a href='Calendario.php?n0w3={$_GET["n0w3"]}'>{$value->getAssunto()}</a></h6>		
				";
			}
		}
		
	}
}
		echo "
		</div>
		<a class='a' href='Principal.php?n0w3={$_GET["n0w3"]}'>Voltar para Tabela</a>";
		echo '</main>
	</body>
	</html>
	';
}else{
    header("Location: Login.php");
}

?>
