<?php
session_start();
if(!isset($_SESSION['log'])){
    $_SESSION['log']=true;
}
require_once("../controle/UsuarioControle.php");
$controle=new ControleUsuario();
$user=new Usuario();
$item=$controle->selecionarTodos();
foreach($item as $atual){
    $emailH=md5($atual['email']);
    if($emailH==$_GET['n0w3']){
        $user->setEmail($atual['email']);
        break;
    }
}
if((isset($_SESSION['sessao']))){
echo "
<!DOCTYPE html>
<html lang='pt-br'>
<head>
    <title>eUp</title>
    <meta charset='utf-8'>
    <link rel='stylesheet' href='menu.css' >
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' href='css/bootstrapi.min.css' integrity='sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO' crossorigin='anonymous'> 
    <link rel='stylesheet' href='css/geral.css'>
    <link rel='icon' type='imagem/png' href='imagem/Logo.svg.png'>
</head>

<body>
	<header>
        <nav class='navbar navbar-expand-md navbar-light fixed-top' id='a'>
        <a class='navbar-brand' id='cor' href='../index.php'><img src='imagem/Logo.svg.png' width='40'>eUp</a>
            <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarCollapse' aria-controls='navbarCollapse' aria-expanded='false' aria-label='Toggle navigation'>
                <span class='navbar-toggler-icon'></span>
            </button>
            <div class='collapse navbar-collapse' id='navbarCollapse'>
                <ul class='navbar-nav mr-auto'>
                    <li class='nav-item active'>
                        <a class='nav-link' id='cor' href='Principal.php?n0w3={$_GET["n0w3"]}'>Projetos</a>
                    </li>
                    <li class='nav-item active'>
                        <a class='nav-link' id='cor' href='Quiz.php?n0w3={$_GET["n0w3"]}'>Quiz</a>
                    </li>
                    <li class='nav-item dropdown'>
            <a id='cor' class='nav-link dropdown-toggle' href='#' id='navbarDropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                Mais
            </a>
            <div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
                
                <a id='cor' class='dropdown-item' href='Configurar.php?n0w3={$_GET["n0w3"]}'>Configurações</a>
                <a id='cor' class='dropdown-item' href='../controle/sair.php'>Sair</a>
            </div>
            </li>
                </ul>
            </div>
        </nav>
    </header>
    <br><br><br>
    <main role='main' class='container'>
      <div class='d-flex align-items-center p-3 my-3 text-white-50 rounded shadow-sm' id='aa'>
        <img src='imagem/help.png' alt='' id='he'>
        <div class='lh-100'>
          <h6 class='mb-0 text-white lh-100'>Dúvidas e Sugestões</h6>          
        </div>
      </div>
      
      <div class='my-3 p-3 bg-white rounded shadow-sm'>
      <h6 class='border-bottom border-gray pb-2 mb-0'>Principais dúvidas.</h6>
      <div class='media text-muted pt-3'>
        <p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>
          <strong class='d-block text-gray-dark'>Qual o intuito do ReUp?</strong>
          Fazer com que você possa organizar seu tempo de maneira simples e divertida, além de aprender a usar a técnica do pomodoro para que você fique craque em gerenciar seu tempo de trabalho e livre.
        </p>
      </div>
      <div class='media text-muted pt-3'>
        <p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>
          <strong class='d-block text-gray-dark'>Como funciona o quiz?</strong>
          O quiz te dará dicas do que fazer usando a técnica do pomodoro para que você possa realizar sua organização com alguma base. O seu resultado ficará salvo até você sair da aplicação ou clicar em recomeçar
        </p>
      </div>
      <div class='media text-muted pt-3'>
        <p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>
          <strong class='d-block text-gray-dark'>O que sou capaz de fazer com o ReUp?</strong>
          O ReUp está voltado para a organização, nele é possível criar projetos que são suas tabelas de organização e gerenciá-las com horários de segunda a sexta. Além disso, é possível marcar datas importantes que são os lembretes, o ReUp irá te avisar quando o dia chegar, mas lembre-se de sempre dar uma olhada na aplicação
        </p>
      </div>
    </div>

    <div class='p-3 bg-white rounded shadow-sm'>
      <h6 class='border-bottom border-gray pb-2 mb-0'>Compartilhe conosco suas sugestões para o nosso crescimento.</h6>
      
        <div class='media text-muted pt-3'>
          <form class='form' method='post' action='../controle/com.php'>
          <div class='form-row'>
            <div class='form-group col-md-7'>
              <label for='exampleInputEmail1'>Obs.: Seu comentário será identificado por seu email.</label>
              <input type='hidden' name='email' value='{$user->getEmail()}' class='form-control' id='exampleInputEmail1' aria-describedby='emailHelp' title='Seu email' />
              <small id='emailHelp' class='form-text text-muted'>Nunca vamos compartilhar seu email, com ninguém.</small>
            </div>          
            <div class='form-group col-md-12'>
              <label for='exampleFormControlTextarea1'>Coloque aqui seus comentários e sugestões</label>
                <textarea class='form-control' id='exampleFormControlTextarea1' name='comen' rows='5'></textarea>
                <small id='emailHelp' class='form-text text-muted'>Seus comentários e sugestões também nunca serão compartilhados.</small>
            </div>
            </div>
            <input type='hidden' name='n0w3' value='{$_GET['n0w3']}' />
            <button type='submit' class='btn' id='btn' >Enviar</button>  
          </form>
          
        </div>
    </div>
    </main>
    <br><br>
    <script src='js/min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
    <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js' integrity='sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy' crossorigin='anonymous'></script>        

</body>
</html>
";
}else{
  header("Location: Login.php");
}
if(isset($_SESSION['erro'])){
    echo"<script src='js/sweet.js'></script>";
    if($_SESSION['erro']){
        echo"<script>swal('Ops','Não foi possível realizar o comentário','error');</script>";
    }else{
        echo"<script>swal('Agradecemos','Seu comentário foi enviado com sucesso','success');</script>";
    }
    unset($_SESSION['erro']);
}
?>
