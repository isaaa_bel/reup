<?php
echo "
<html>
<head>
    <title>Pomodoro</title>
    <meta charset='utf-8'>
    <link rel='stylesheet' href='menu.css' >
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' href='css/bootstrap.css' crossorigin='anonymous'>
    <link rel='stylesheet' href='css/geral.css'>
    <link rel='icon' type='imagem/png' href='imagem/Logo.svg.png'>
    <style>
        .container{
            background-color: white;
            border-radius: 7px;
            -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);

        }
        .texto{
            font-size: 20px;
        }
        .imagem{
            position: center;
        }
        .corpo{
            background-color: #afeeee !important;
        }
        #mT{
            float: right;            
        }
    </style>
</head>
<body id='teste' class='corpo'>
<br><br>
    <div class='container'>
        <center><img src='imagem/curioso.jpg' width='150' height='150' class='imagem'>
        ";
        echo "
        <p style='font-size:30px; text-align:right; font-family:verdana;'>
          <span id='timer_minutos'>25</span> :
          <span id='timer_segundos'>00</span>
        </p>        
        <script type='text/javascript'>
            function Contador(m_init){
                var t_minutos = document.getElementById('timer_minutos');
                var t_segundos = document.getElementById('timer_segundos');
                t_minutos.innerHTML = ((m_init - 1) > 9) ? ('' + (m_init - 1)) : ('0' + (m_init - 1));
                t_segundos.innerHTML = '59';
                var m = m_init - 1;
                var s = '59';
                var count = setInterval(function(){
                    t_minutos.innerHTML = (m > 9) ? ('' + m) : ('0' + m);
                    t_segundos.innerHTML = (s > 9) ? ('' + s) : ('0' + s);
                    if(s > 0){
                        s -= 1;
                    }else if(s == 0 && m > 0){
                        s = 59;
                        m -= 1;
                    }else{
                        m = m_init;
                    }
                }, 1000);
            }
            Contador(25);
        </script>
";
echo "
    </div>
    <br>
    <div class='container'>
        </br>
        <h2 align='center'>O que é a técnica Pomodoro?</h2>
        <br>
        <p class='texto'>A técnica Pomodoro (tomate, em italiano) foi nomeada pelo seu criador Francesco Cirillo,
         em homenagem ao seu cronômentro em forma de tomate que ele costumava utilizar para acompanhar
         seus estudos universitários. Pomodoro é o seu total, que será divididos em pomodoris de 30 minutos cada.
         Consiste em dividir seu tempo, realizando atividades de 25 minutos e descansando 5 minutos
        entre elas, e assim sucessivamente até completar seu cronograma. Dessa forma, não ocorre desgaste físico ou mental.</p>
        <br>
        <h2 align='center'>Seu tempo?</h2>
        <p class='texto'>Você poderá usar essa página para marcar seu tempo importante enquanto você estuda, ao final dos seus
        25 minutos para estudo você será redirecionado para sua pausa de 5 minutos, aproveite e não se desgaste!</p>
        <strong style='color: red'>Atenção:</strong> Se você sair ou atualizar essa página, a contagem reiniciará, portanto se estiver levando essa técnica a sério, permaneça na página.
        <br><br>
        <span id='mT'>Para saber mais, fique a vontade clicando aqui <a target='blank' href='https://descomplica.com.br/tudo-sobre-pos/gestao/como-funciona-a-tecnica-pomodoro/?utm_source=ads_google&utm_medium=search&utm_campaign=psq-nonbrand-posgrad-encher-publicidade-awareness-ongoing-trafego-dsa&device=c&network=g&utm_content=337968391625&matchtype=b&utm_term=Blog&gclid=CjwKCAjwgbLzBRBsEiwAXVIygLWVPolHNiBNaBQPYw0tsBbwVJPBsxsMn-E4D9_UQ6xadyjm8qfgRBoCp-YQAvD_BwE' title='Para saber mais sobre a técnica do pomodoro? Clique aqui'>pomodoro!</a></p></span>
        <br><br>        
        <input type='hidden' value='{$_GET['n0w3']}' id='n0w3' />
    </div>
    <br>
</body>
<script src='js/sweet.js'></script>
<script src='js/cor.js'></script>
<script src='js/tes.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js' integrity='sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl'
crossorigin='anonymous'></script>

</html>";
?>
