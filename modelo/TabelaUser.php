<?php
class TabelaUser{
    private $id;
    private $hora;
    private $usuario;
    private $projeto;
    private $campo;
    private $valor;
    private $segunda;
    private $terca;
    private $quarta;
    private $quinta;
    private $sexta;
    public function setCampo($campo){
        $this->campo=$campo;
    }
    public function getCampo(){
        return $this->campo;
    }
    public function setValor($valor){
        $this->valor=$valor;
    }
    public function getValor(){
        return $this->valor;
    }
    public function setId($id){
        $this->id=$id;
    }
    public function getId(){
        return $this->id;
    }
    public function setHora($hora){
        $this->hora=$hora;
    }
    public function getHora(){
        return $this->hora;
    }
    public function setUsuario($usuario){
        $this->usuario=$usuario;
    }
    public function getUsuario(){
        return $this->usuario;
    }
    public function setProjeto($projeto){
        $this->projeto=$projeto;
    }
    public function getProjeto(){
        return $this->projeto;
    }
    public function setSegunda($segunda){
        $this->segunda=$segunda;
    }
    public function getSegunda(){
        return $this->segunda;
    }
    public function setTerca($terca){
        $this->terca=$terca;
    }
    public function getTerca(){
        return $this->terca;
    }
    public function setQuarta($quarta){
        $this->quarta=$quarta;
    }
    public function getQuarta(){
        return $this->quarta;
    }
    public function setQuinta($quinta){
        $this->quinta=$quinta;
    }
    public function getQuinta(){
        return $this->quinta;
    }
    public function setSexta($sexta){
        $this->sexta=$sexta;
    }
    public function getSexta(){
        return $this->sexta;
    }
}
?>
