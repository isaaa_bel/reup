<?php
    class Usuario{        
        private $nome;
        private $email;
        private $senha;
        private $confSenha;
        public function getNome(){
            return $this->nome;
        }
        public function getEmail(){
            return $this->email;
        }
        public function getSenha(){
            return $this->senha;
        }
        public function setNome($n){
            $this->nome = $n;
        }
        public function setEmail($e){
            $this->email = $e;
        }
        public function setSenha($s){
            $this->senha = $s;
        }
        public function getConfSenha(){
            return $this->confSenha;
        }
        public function setConfSenha($cs){
            $this->confSenha = $cs;
        }

    }
?>
