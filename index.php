<?php
session_start();
  echo"
<!DOCTYPE html>
<html lang='pt-br'>
<head>
  <title>Home | Bem vindo ao Reup</title>
  <meta charset='utf-8'>
  <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
  <link rel='stylesheet' href='visual/css/bootstrapi.min.css' integrity='sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO' crossorigin='anonymous'>  
  <link rel='icon' type='imagem/png' href='visual/imagem/Logo.svg.png'>
<style>
hr{ 
  margin-top: 10%;
}

#r{
  font-size: 100px;
  margin-left: 90%;
  font-family: fira-code;
  position: absolute;
}
#img{
  animation-duration: 3.5s;
  animation-name: slidein;
}
#corre{
    animation: linear infinite;
    animation-duration: 3.4s;
    animation-name: teste;
}

@keyframes teste {
  from {
    margin-left: -100%;
    width: 100%
  }

  to {
    margin-left: 320%;
    width: 100%;
  }
}

@keyframes slidein {
  from {
    margin-left: -200%;    
  }

  to {
    margin-left: 0%;
  }
}

#u{
  font-size: 100px;
  position: absolute;
  font-family: fira-code;   
  margin-top: 34%;
  margin-left: 2%;
}

.teste{
    background-color: #bee3ff;
    margin-bottom: 10%;
}
#a{
    background-color: #bee3ff;
}
.r{
    border-radius: 10px;
}

.fundo{
  background-color: #cdfffe;
  margin-top: 2%;
  margin-bottom: 10%;
}

#cor{
    color: #000;
}
.sep{
  margin-right: 100px;
}

@media(max-width: 700px){
    #corre{
    animation: linear infinite;
    animation-duration: 1.2s;
    animation-name: teste;
}

@keyframes teste {
  from {
    margin-left: -100%;
    width: 100%
  }

  to {
    margin-left: 2%;
    width: 100%;
  }
}
}

.Redes{
  margin-left: 25px;
  font-size: 30px;
}

</style>
</head>
<body>
  <!-- Menu-->
    <header>
      <nav class='navbar navbar-expand-md navbar-light fixed-top' id='a'>
      <a class='navbar-brand' id='cor' href='#'>
      <img src='visual/imagem/Logo.svg.png' width='48'>eUp</a>
        <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarCollapse' aria-controls='navbarCollapse' aria-expanded='false' aria-label='Toggle navigation'>
          <span class='navbar-toggler-icon'></span>
          </button>
        <div class='collapse navbar-collapse' id='navbarCollapse'>
          <ul class='navbar-nav mr-auto'>
            <li class='nav-item active'>
              <a class='nav-link' id='cor' href='visual/Login.php'>Login </a>
            </li>
            <li class='nav-item active'>
              <a class='nav-link' id='cor' href='visual/Cadastro.php'>Cadastre-se</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <br /><br />

    <main role='main'>
      <div class='carousel-inner'>
      <div class='teste'>      
        <div class='carousel-item active'>
        <br><br>
          <center><img id='img' class='d-block w-75 h-100' src='visual/imagem/rela.jpg' alt='First slide'>
        </div>        
        </div>    

<!-- Corpo-->
    <div class='container marketing'>       
        <div class='row'>
            <div class='col-md-6'>
              <h2 class='featurette-heading'> O que é o ReUp<span class='text-muted'>, afinal?</span></h2>                
              <p class='lead sep'>Um site que vai te auxiliar a deixar a procrastinação de lado e colocar a mão na massa, atráves de organizações por uma tabela que vai
                te ajudar a fazer tudo no tempo adequado com um método diferente conhecido como técnica do <a target='blank' href='visual/Pomodoro.php' title='Para saber mais sobre a técnica do pomodoro? Clique aqui'>pomodoro!</a></p>                
            </div>    
              <div class='col-md-6'>
                <h2 class='featurette-heading'>Correr contra o tempo<span class='text-muted'>?</span></h2>
                <p class='lead sep'>Nosso principal objetivo é organizar o seu tempo de uma forma dinâmica e diferente que pode ser
                feito por você mesmo e atraves de um quiz. Te ajudar
                com lembretes e tudo que você precisar.</p>
              </div>
              
            </div>
            
            </div>
          </div>          
          
          <div class='col-md-3 m'>                                
              <img id='corre' src='visual/imagem/boneco.gif' border='0' width='380' alt='quiz' />
              </div>

            <!-- Div azul-->
<div class='fundo'>
  <div class='container marketing'>
  <br><br>
    <div class='row featurette'>
      <div class='col-md-6'>
      <br>
        <h2 class='featurette-heading'>Questionário<span class='text-muted'>?</span></h2>
        <br>
        <p class='lead sep'>Sim, mas relaxe, não é aquele questionário cansativo que você precisa de paciência para responder. Na verdade você pode organizar seu tempo de
        uma maneira divertida e dinâmica atráves dele, simplesmente respondendo nossas perguntas e vualá, terá uma melhor forma para organizar sua tabela!</p>
      </div>
      <div class='col-md-5'>
        <img class='r featurette-image img-fluid mx-auto' src='visual/imagem/q.jpeg' alt='Generic placeholder image'>
      </div>
    </div>
    <br><br>
  </div>
</div>

      <div class='container marketing'>
      
        <div class='row featurette'>
          <div class='col-md-4 order-md-4'>
          <br><br><br>
            <h2 class='featurette-heading'Afinal, o que é o ReUp? <span class='text-muted'>Não perca tempo!</span></h2>
            <p class='lead'>Administrar o tempo e a quantidade de coisas a fazer é uma chave para o sucesso, pois dessa maineira você consegue utilizar mais tempo com o que é realmente importante.</p>
          </div>
          
          <div class='col-md-7 order-md-1'>
            <img class='featurette-image img-fluid mx-auto' src='visual/imagem/imagem2.svg' alt='Generic placeholder image'>
          </div>
        </div>
                
        <hr class='featurette-divider'>
        <p class='float-right'><a href='#'>Voltar ao topo</a></p>
        <p> ReUp, 2020  <a href='visual/Login.php'>Login</a> &middot; <a href='visual/Cadastro.php'>Cadastre-se</a></p>
      </div>                    
    </main>
  <script src='visual/js/sweet.js'></script>  
  <script src='visual/js/min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
  <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js' integrity='sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy' crossorigin='anonymous'></script>        
  </body>
</html>
";
if(isset($_SESSION['erroDel'])){
    if(!$_SESSION['erroDel']){
        echo"<script>swal('Conta deletada','Sua conta junto com seus dados foram apagados do sistema','info');</script>";
    }
    session_destroy();
}
if(isset($_SESSION['erroB'])){
    echo"<script>swal('Desculpe','Estamos com problemas no servidor, tente novamente','info');</script>";
    session_destroy();
}
?>
