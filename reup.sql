DROP SCHEMA IF EXISTS reup;
CREATE SCHEMA IF NOT EXISTS reup;

CREATE TABLE reup.usuario(
    id INT PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(100)  NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    senha VARCHAR(100) NOT NULL
);

CREATE TABLE reup.calendario(
	id INT(4) PRIMARY KEY AUTO_INCREMENT,
    usuario VARCHAR(100) NOT NULL,
	assunto VARCHAR(100) NOT NULL,
	mensagem VARCHAR(100) NOT NULL,
	dataa VARCHAR(10) NOT NULL,
	concluir VARCHAR(10) NOT NULL,
    FOREIGN KEY(usuario) REFERENCES usuario(email)
);

CREATE TABLE reup.projeto(
    id INT(4) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL,
    descricao VARCHAR(200) NOT NULL
);

CREATE TABLE reup.tabela(
    id INT(4) PRIMARY KEY AUTO_INCREMENT,
    usuario VARCHAR(100) NOT NULL,
    projeto INT(4) NOT NULL,
    hora VARCHAR(50),
    segunda VARCHAR(50),
    terca VARCHAR(50),
    quarta VARCHAR(50),
    quinta VARCHAR(50),
    sexta VARCHAR(50),
    FOREIGN KEY(usuario) REFERENCES usuario(email),
    FOREIGN KEY(projeto) REFERENCES projeto(id)
);

CREATE TABLE reup.comentario(
    id INT(4) PRIMARY KEY AUTO_INCREMENT,    
    email VARCHAR(100) NOT NULL,
    comentario VARCHAR(10000) NOT NULL
);
INSERT INTO reup.usuario(nome,email,senha) VALUES('Developers','equipe_oficial@reup.com','reup123');
