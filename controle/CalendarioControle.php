<?php 
	require_once("../modelo/CalendarioModelo.php");
	require_once("Conexao.php");

	class CalendarioControle{
		function inserir($calendario){
            try{
                $con=new Conexao();
                $comando=$con->getConexao()->prepare("INSERT INTO calendario(usuario,assunto,mensagem,dataa,concluir) VALUES (:usuario,:assunto,:mensagem,:dataa,:concluir);");
                $user=$calendario->getUsuario();
                $assunto = $calendario->getAssunto();
                $mensagem= $calendario->getMensagem();                
                $dataa= $calendario->getDataa();
                $concluir= $calendario->getConcluir(); 
                $comando->bindParam("usuario",$user);
                $comando->bindParam("assunto",$assunto);
                $comando->bindParam("mensagem",$mensagem);
                $comando->bindParam("dataa",$dataa);
                $comando->bindParam("concluir",$concluir);                
                if($comando->execute()){
                    $con->fecharConexao();
                    return true;
                }
                else{
                    $con->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }
        
        function selecionar(){
        	try {
        		$con=new Conexao();
                $comando=$con->getConexao()->prepare("SELECT * FROM calendario"); 
                $comando->execute();
                $result = $comando->fetchAll(PDO::FETCH_CLASS,"CalendarioModelo");
                return $result;
        	} catch (Exception $e) {
        		echo"Erro geral: {$e->getMessage()}";
        		
        	} catch(PDOException $e){
        		echo"Erro PDO: {$e->getMessage()}";
        	}
        }
        function update($calendario){
            try {
                $conexao = new Conexao();
                $id = $calendario->getId();
                $concluir = $calendario->getConcluir();
                $cmd = $conexao->getConexao()->prepare("UPDATE calendario SET concluir=:concluir WHERE id=:id");
                $cmd->bindParam("concluir", $concluir);
                $cmd->bindParam("id", $id);
                if($cmd->execute()){
                        $conexao->fecharConexao();
                        return true;
                    }else{
                        $conexao->fecharConexao();
                        return false;
                    }
            } catch (Exception $e) {
                echo "Erro do geral: {$e->getMessage()}";
                return false;
            } catch (PDOException $e) {
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }
        }
        function deletar($id){
            try {
                $con = new Conexao();
                $comando= $con->getConexao()->prepare("DELETE FROM calendario WHERE id=:id");
                $comando->bindParam("id",$id);
                if($comando->execute()){
                    $con->fecharConexao();
                    return true;
                }else{
                    $con->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }
	}
?>
