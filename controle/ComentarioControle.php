<?php
    require_once("../modelo/Comentario.php");
    require_once("../modelo/Usuario.php");
    require_once("Conexao.php");
    class ComentarioControle{
        function inserir($com,$user){
            try{
                $con=new Conexao();
                $comando=$con->getConexao()->prepare("INSERT INTO comentario(email,comentario) VALUES (:email,:com);");
                $email=$user->getEmail();                
                $com = $com->getCom();                                
                $comando->bindParam("email",$email);
                $comando->bindParam("com",$com);                
                if($comando->execute()){
                    $con->fecharConexao();
                    return true;
                }
                else{
                    $con->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }
        function selecionarTodos(){
            try{
                $con=new Conexao();
                $comando=$con->getConexao()->prepare("SELECT * FROM comentario");
                if($comando->execute()){
                    $rse=$comando->fetchall();
                    $con->fecharConexao();
                    return $rse;
                }else{
                    $con->fecharConexao();
                    return null;
                }
            }catch(PDOException $e){
                echo"Erro do banco: {$e->getMessage()}";
                return null;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return null;
            }
        }
        function deletar($comentario){
            try{
                $con=new Conexao();
                $cmd=$con->getConexao()->prepare("DELETE FROM comentario WHERE id=:id;");
                $id=$comentario->getId();
                $cmd->bindParam("id",$id);
                if($cmd->execute()){
                    $con->fecharConexao();
                    return true;
                }else{
                    $con->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo"Erro no banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo"Erro geral: {$e->getMessage()}";
                return false;
            }
        }
    }
?>
