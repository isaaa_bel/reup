<?php
require_once("Conexao.php");
require_once("../modelo/TabelaUser.php");
class TabelaUserControle{
    public function selecionarTodos(){
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("SELECT * FROM tabela;");
            if($cmd->execute()){
                $resultado=$cmd->fetchall(PDO::FETCH_CLASS,"TabelaUser");
                $con->fecharConexao();
                return $resultado;
            }else{
                $con->fecharConexao();
                return null;
            }
        }catch(PDOException $e){
            echo"Erro da biblioteca: {$e->getMessage()}";
        }catch(Exception $e){
            echo"Erro geral: {$e->getMessage()}";
        }
    }
    public function selecionarPorUser($tabela){
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("SELECT * FROM tabela WHERE usuario=:user ORDER BY projeto;");
            $user=$tabela->getUsuario();
            $cmd->bindParam("user",$user);
            if($cmd->execute()){
                $rse=$cmd->fetchall();
                $con->fecharConexao();
                return $rse;
            }else{
                $con->fecharConexao();
                return null;
            }
        }catch(PDOException $e){
            echo"Erro no banco: {$e->getMessage()}";
            return null;
        }catch(Exception $e){
            echo"Erro geral: {$e->getMessage()}";
            return null;
        }
    }
    public function selecionarAvancado($tabela){
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("SELECT * FROM tabela WHERE usuario=:user AND projeto=:projeto;");
            $user=$tabela->getUsuario();
            $projeto=$tabela->getProjeto();
            $cmd->bindParam("user",$user);
            $cmd->bindParam("projeto",$projeto);
            if($cmd->execute()){
                $rse=$cmd->fetchall();
                $con->fecharConexao();
                return $rse;
            }else{
                $con->fecharConexao();
                return $rse;
            }
        }catch(PDOException $e){
            echo"Erro no banco: {$e->getMessage()}";
            return null;
        }catch(Exception $e){
            echo"Erro geral: {$e->getMessage()}";
            return null;
        }
    }
    public function inserir($tabela){
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("INSERT INTO tabela(usuario,projeto) VALUES(:user,:projeto);");
            $user=$tabela->getUsuario();
            $projeto=$tabela->getProjeto();
            $cmd->bindParam("user",$user);
            $cmd->bindParam("projeto",$projeto);
            if($cmd->execute()){
                $con->fecharConexao();
                return true;
            }else{
                $con->fecharConexao();
                return false;
            }
        }catch(PDOException $e){
            echo"Erro na biblioteca: {$e->getMessage()}";
        }catch(Exception $e){
            echo"Erro geral: {$e->getMessage()}";
        }
    }
    public function atualizar($tabela){
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("UPDATE tabela SET ".$tabela->getCampo()."=:valor WHERE id=:id;");
            $campo=$tabela->getCampo();
            $valor=$tabela->getValor();
            $id=$tabela->getId();
            $cmd->bindParam("valor",$valor);
            $cmd->bindParam("id",$id);
            if($cmd->execute()){
                $con->fecharConexao();
                return true;
            }else{
                $con->fecharConexao();
                return false;
            }
        }catch(PDOException $e){
            echo"Erro no banco: {$e->getMessage()}";
            return true;
        }catch(Exception $e){
            echo"Erro geral: {$e->getMessage()}";
            return false;
        }
    }
    public function deletarLinha($tabela){
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("DELETE FROM tabela WHERE id=:id;");
            $id=$tabela->getId();
            $cmd->bindParam("id",$id);
            if($cmd->execute()){
                $con->fecharConexao();
                return true;
            }else{
                $con->fecharConexao();
                return false;
            }
        }catch(PDOException $e){
            echo"Erro no banco: {$e->getMessage()}";
            return false;
        }catch(Exception $e){
            echo"Erro geral: {$e->getMessage()}";
            return false;
        }
    }
    public function deletarProjeto($tabela){
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("DELETE FROM tabela WHERE projeto=:projeto;");
            $projeto=$tabela->getProjeto();
            $cmd->bindParam("projeto",$projeto);
            if($cmd->execute()){
                $con->fecharConexao();
                return true;
            }else{
                $con->fecharConexao();
                return false;
            }
        }catch(PDOException $e){
            echo"Erro no banco: {$e->getMessage()}";
            return true;
        }catch(Exception $e){
            echo"Erro geral: {$e->getMessage()}";
            return false;
        }
    }
    public function deletarPorUser($tabela){
        try{
            $con=new Conexao();
            $cmd=$con->getConexao()->prepare("DELETE FROM tabela WHERE usuario=:usuario");
            $usuario=$tabela->getUsuario();
            $cmd->bindParam("usuario",$usuario);
            if($cmd->execute()){
                $con->fecharConexao();
                return true;
            }else{
                $con->fecharConexao();
                return false;
            }
        }catch(PDOException $e){
            echo"Erro no banco: {$e->getMessage()}";
            return false;
        }catch(Exception $e){
            echo"Erro geral: {$e->getMessage()}";
            return false;
        }
    }
}
?>
