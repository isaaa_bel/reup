<?php
require_once("../modelo/Usuario.php");
require_once("Conexao.php");
    class ControleUsuario{
        function selecionarTodos(){
            try{
                $con=new Conexao();
                $cmd=$con->getConexao()->prepare("SELECT * FROM usuario;");
                if($cmd->execute()){
                    $resultado=$cmd->fetchall();
                    $con->fecharConexao();
                    return $resultado;
                }else{
                    $con->fecharConexao();
                    return null;
                }
            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
          }

        function inserir($usuario){
            try{
                $con=new Conexao();
                $comando=$con->getConexao()->prepare("INSERT INTO usuario(nome,email,senha) VALUES (:n,:e,:s);");
                $nome = $usuario->getNome();
                $email= $usuario->getEmail();
                $senha= $usuario->getSenha();
                $comando->bindParam("n",$nome);
                $comando->bindParam("e",$email);
                $comando->bindParam("s",$senha);
                if($comando->execute()){
                    $con->fecharConexao();
                    return true;
                }
                else{
                    $con->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }

        function selecionarUm($usuario){
            try{
                $con=new Conexao();
                $comando=$con->getConexao()->prepare("SELECT * FROM usuario WHERE email=?;");
                $user=$usuario->getEmail();
                $comando->bindParam(1,$user);
                if($comando->execute()){
                    $resultado=$comando->fetchAll(PDO::FETCH_CLASS,"Usuario");
                    $con->fecharConexao();
                    return $resultado;
                }else{
                    $con->fecharConexao();
                    return NULL;
                }

            }
            catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                $con->fecharConexao();
                return NULL;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                $con->fecharConexao();
                return NULL;
            }
        }

        function atualizar($usuario,$oldEmail){
            try{
                $con=new Conexao();
                $cmd=$con->getConexao()->prepare("UPDATE usuario SET nome=:n,senha=:s WHERE email=:oldEmail ");
                $nome=$usuario->getNome();
                $senha=$usuario->getSenha();
                $cmd->bindParam("n",$nome);
                $cmd->bindParam("s",$senha);
                $cmd->bindParam("oldEmail",$oldEmail);
                if($cmd->execute()){
                    $con->fecharConexao();
                    return true;
                }else{
                    $con->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo"Erro no banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo"Erro geral: {$e->getMessage()}";
                return false;
            }
        }
        function deletar($usuario){
            try{
                $con=new Conexao();
                $cmd=$con->getConexao()->prepare("DELETE FROM usuario WHERE email=:e");
                $email=$usuario->getEmail();
                $cmd->bindParam("e",$email);
                if($cmd->execute()){
                    $con->fecharConexao();
                    return true;
                }else{
                    $con->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo"Erro no banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo"Erro geral: {$e->getMessage()}";
                return false;
            }
        }
    }
?>
