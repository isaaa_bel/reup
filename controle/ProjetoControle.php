<?php
require_once("../modelo/Projeto.php");
require_once("Conexao.php");
    class ProjetoControle{
        public function inserir($projeto){
            try{
                $con=new Conexao();
                $cmd=$con->getConexao()->prepare("INSERT INTO projeto(nome,descricao) VALUES(:n,:d);");
                $nome=$projeto->getNome();
                $descricao=$projeto->getDescricao();
                $cmd->bindParam("n",$nome);
                $cmd->bindParam("d",$descricao);
                if($cmd->execute()){
                    $con->fecharConexao();
                    return true;
                }else{
                    $con->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo"Erro no banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo"Erro geral: {$e->getMessage()}";
                return false;
            }
        }
        public function selecionarUm($projeto){
            try{
                $con=new Conexao();
                $cmd=$con->getConexao()->prepare("SELECT * FROM projeto WHERE id=:id");
                $id=$projeto->getId();
                $cmd->bindParam("id",$id);
                if($cmd->execute()){
                    $rse=$cmd->fetchall();
                    $con->fecharConexao();
                    return $rse;
                }else{
                    $con->fecharConexao();
                    return null;
                }
            }catch(PDOException $e){
                echo"Erro no banco: {$e->getMessage()}";
                return null;
            }catch(Exception $e){
                echo"Erro geral: {$e->getMessage()}";
                return null;
            }
        }
        public function selecionarTodos(){
            try{
                $con=new Conexao();
                $cmd=$con->getConexao()->prepare("SELECT * FROM projeto;");
                if($cmd->execute()){
                    $rse=$cmd->fetchall();
                    $con->fecharConexao();
                    return $rse;
                }else{
                    $con->fecharConexao();
                    return $rse;
                }
            }catch(PDOException $e){
                echo"Erro no banco: {$e->getMessage()}";
                return null;
            }catch(Exception $e ){
                echo"Erro geral: {$e->getMessage()}";
                return null;
            }
        }
        public function deletar($projeto){
            try{
                $con=new Conexao();
                $cmd=$con->getConexao()->prepare("DELETE FROM projeto WHERE id=:id");
                $id=$projeto->getId();
                $cmd->bindParam("id",$id);
                if($cmd->execute()){
                    $con->fecharConexao();
                    return true;
                }else{
                    $con->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo"Erro no banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo"Erro geral: {$e->getMessage()}";
                return false;
            }
        }
    }
?>
